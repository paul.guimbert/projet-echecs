import random

#Programme très basique ou l'ordi choisi un coup au hasard parmi les coups légaux

def coup_possiblement_legal(case_actuelle, last_move, cases_occupied_l, checked, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
    ligne = int(case_actuelle[1])-1
    coups_possibles = []
    for i in range(8):
        if case_actuelle == d_board_bis[ligne][i][0]:
            colonne = i
    if d_board_bis[ligne][colonne][1] == "tour" or d_board_bis[ligne][colonne][1] == "reine":
        i=colonne + 1
        while i<8:
            if d_board_bis[ligne][i][1] == None:
                coups_possibles.append(d_board_bis[ligne][i][0])
                i+=1
            else:
                if d_board_bis[ligne][i][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[ligne][i][0])
                i=8
        i = colonne -1
        while i>=0:
            if d_board_bis[ligne][i][1] == None:
                coups_possibles.append(d_board_bis[ligne][i][0])
                i-=1
            else:
                if d_board_bis[ligne][i][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[ligne][i][0])
                i=-1
        j = ligne + 1
        while j<8:
            if d_board_bis[j][colonne][1] == None:
                coups_possibles.append(d_board_bis[j][colonne][0])
                j+=1
            else:
                if d_board_bis[j][colonne][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[j][colonne][0])
                j=8
        j = ligne -1
        while j>=0:
            if d_board_bis[j][colonne][1] == None:
                coups_possibles.append(d_board_bis[j][colonne][0])
                j-=1
            else:
                if d_board_bis[j][colonne][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[j][colonne][0])
                j=-1
    elif d_board_bis[ligne][colonne][1] == "cavalier":
        col_a = colonne + 2
        col_b = colonne + 1
        col_c = colonne - 1
        col_d = colonne - 2
        lig_a = ligne + 2
        lig_b = ligne + 1
        lig_c = ligne - 1
        lig_d = ligne - 2
        if col_a < 8:
            if lig_b < 8:
                if d_board_bis[lig_b][col_a][1] == None or d_board_bis[lig_b][col_a][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_b][col_a][0])
            if lig_c >= 0:
                if d_board_bis[lig_c][col_a][1] == None or d_board_bis[lig_c][col_a][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_c][col_a][0]) 
        if col_d >= 0:
            if lig_b < 8:
                if d_board_bis[lig_b][col_d][1] == None or d_board_bis[lig_b][col_d][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_b][col_d][0])
            if lig_c >= 0:
                if d_board_bis[lig_c][col_d][1] == None or d_board_bis[lig_c][col_d][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_c][col_d][0])
        if col_b < 8:
            if lig_a < 8:
                if d_board_bis[lig_a][col_b][1] == None or d_board_bis[lig_a][col_b][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_a][col_b][0])
            if lig_d >= 0:
                if d_board_bis[lig_d][col_b][1] == None or d_board_bis[lig_d][col_b][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_d][col_b][0]) 
        if col_c >= 0:
            if lig_a < 8:
                if d_board_bis[lig_a][col_c][1] == None or d_board_bis[lig_a][col_c][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_a][col_c][0])
            if lig_d >= 0:
                if d_board_bis[lig_d][col_c][1] == None or d_board_bis[lig_d][col_c][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_d][col_c][0])
    if d_board_bis[ligne][colonne][1] == "fou" or d_board_bis[ligne][colonne][1] == "reine":
        a=ligne+1
        b=colonne+1
        while a<8 and b<8:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a+=1
                b+=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=8
                b=8
        a=ligne+1
        b=colonne-1
        while a<8 and b>=0:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a+=1
                b-=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=8
                b=-1
        a=ligne-1
        b=colonne-1
        while a>=0 and b>=0:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a-=1
                b-=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=-1
                b=-1
        a=ligne-1
        b=colonne+1
        while a>=0 and b<8:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a-=1
                b+=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=-1
                b=8
    elif d_board_bis[ligne][colonne][1] == "pion":
        if d_board_bis[ligne][colonne][3] == 1:
            if ligne<7:
                if ligne == 1:
                    if d_board_bis[ligne+1][colonne][1] == None and d_board_bis[ligne+2][colonne][1] == None:
                        coups_possibles.append(d_board_bis[ligne+2][colonne][0])
                elif ligne == 4:
                    if colonne + 1 <8:
                        if last_move[0]==d_board_bis[ligne+2][colonne+1][0] and last_move[1]==d_board_bis[ligne][colonne+1][0] and last_move[2]=="pion":
                            coups_possibles.append(d_board_bis[ligne+1][colonne+1][0])
                    if colonne - 1 >=0:
                        if last_move[0]==d_board_bis[ligne+2][colonne-1][0] and last_move[1]==d_board_bis[ligne][colonne-1][0] and last_move[2]=="pion":
                            coups_possibles.append(d_board_bis[ligne+1][colonne-1][0])
                if d_board_bis[ligne+1][colonne][1] == None:
                    coups_possibles.append(d_board_bis[ligne+1][colonne][0])
                if colonne+1<8:
                    if d_board_bis[ligne+1][colonne+1][3] == 2:
                        coups_possibles.append(d_board_bis[ligne+1][colonne+1][0])
                if colonne-1>=0:
                    if d_board_bis[ligne+1][colonne-1][3] == 2:
                        coups_possibles.append(d_board_bis[ligne+1][colonne-1][0])
        elif d_board_bis[ligne][colonne][3] == 2:
            if ligne == 6:
                if d_board_bis[ligne-1][colonne][1] == None and d_board_bis[ligne-2][colonne][1]:
                    coups_possibles.append(d_board_bis[ligne-2][colonne][0])
            elif ligne == 3:
                if colonne + 1 <8:
                    if last_move[0]==d_board_bis[ligne-2][colonne+1][0] and last_move[1]==d_board_bis[ligne][colonne+1][0] and last_move[2]=="pion":
                        coups_possibles.append(d_board_bis[ligne-1][colonne+1][0])
                if colonne - 1 >=0:
                    if last_move[0]==d_board_bis[ligne-2][colonne-1][0] and last_move[1]==d_board_bis[ligne][colonne-1][0] and last_move[2]=="pion":
                        coups_possibles.append(d_board_bis[ligne-1][colonne-1][0])
            if d_board_bis[ligne-1][colonne][1] == None:
                coups_possibles.append(d_board_bis[ligne-1][colonne][0])
            if colonne+1<8:
                if d_board_bis[ligne-1][colonne+1][3] == 1:
                    coups_possibles.append(d_board_bis[ligne-1][colonne+1][0])
            if colonne-1>=0:
                if d_board_bis[ligne-1][colonne-1][3] == 1:
                    coups_possibles.append(d_board_bis[ligne-1][colonne-1][0])
    if d_board_bis[ligne][colonne][1] == "roi":
        for i in range(-1,2):
            for j in range(-1,2):
                if i!=0 or j!=0:
                    if ligne+i>=0 and ligne+i<8 and colonne+j>=0 and colonne+j<8:
                        if d_board_bis[ligne+i][colonne+j][3]==None or d_board_bis[ligne+i][colonne+j][3]!=d_board_bis[ligne][colonne][3]:
                            case_possible = d_board_bis[ligne+i][colonne+j][0]
                            if last_move != None:
                                if last_move[2]!="roi":
                                    prise_roi = 0
                                    for case in cases_occupied_l:
                                        ligne_o = int(case[1])-1
                                        for p in range(8):
                                            if case == d_board_bis[ligne_o][p][0]:
                                                colonne_o = p
                                        if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                                            cases_occupied_bis = cases_occupied_l.copy()
                                            cases_occupied_bis[case_possible]=cases_occupied_l[case_actuelle]
                                            del cases_occupied_bis[case_actuelle]
                                            ligne_possible = ligne+i
                                            colonne_possible = colonne+j
                                            prem = d_board_bis[ligne_possible][colonne_possible][1]
                                            deux = d_board_bis[ligne_possible][colonne_possible][2]
                                            trois = d_board_bis[ligne_possible][colonne_possible][3]
                                            d_board_bis[ligne_possible][colonne_possible][1]=d_board_bis[ligne][colonne][1]
                                            d_board_bis[ligne_possible][colonne_possible][2]=d_board_bis[ligne][colonne][2]
                                            d_board_bis[ligne_possible][colonne_possible][3]=d_board_bis[ligne][colonne][3]
                                            d_board_bis[ligne][colonne][1] = None
                                            d_board_bis[ligne][colonne][2] = None
                                            d_board_bis[ligne][colonne][3] = None
                                            if case_possible in coup_possiblement_legal(case, [case_actuelle, case_possible, "roi"], cases_occupied_bis, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                                prise_roi = 1
                                            d_board_bis[ligne][colonne][1]=d_board_bis[ligne_possible][colonne_possible][1]
                                            d_board_bis[ligne][colonne][2]=d_board_bis[ligne_possible][colonne_possible][2]
                                            d_board_bis[ligne][colonne][3]=d_board_bis[ligne_possible][colonne_possible][3]
                                            d_board_bis[ligne_possible][colonne_possible][1] = prem
                                            d_board_bis[ligne_possible][colonne_possible][2] = deux
                                            d_board_bis[ligne_possible][colonne_possible][3] = trois
                                    if prise_roi == 0:
                                        coups_possibles.append(d_board_bis[ligne+i][colonne+j][0])
                                else:
                                    coups_possibles.append(d_board_bis[ligne+i][colonne+j][0])
        if d_board_bis[ligne][colonne][3]==1 and not king_moved[0] and checked==0:
            if "f1" not in cases_occupied_l and "g1" not in cases_occupied_l and not king_side_rook_moved[0]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "f1" in coup_possiblement_legal(case, [case_actuelle, "g1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "g1" in coup_possiblement_legal(case, [case_actuelle, "g1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "h1" in coup_possiblement_legal(case, [case_actuelle, "g1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="g2" or d_board_bis[ligne_o][colonne_o][0]=="h2":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("g1")
            elif "b1" not in cases_occupied_l and "c1" not in cases_occupied_l and "d1" not in cases_occupied_l and not queen_side_rook_moved[0]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "a1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "b1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "c1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "d1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="a2" or d_board_bis[ligne_o][colonne_o][0]=="b2" or d_board_bis[ligne_o][colonne_o][0]=="c2":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("c1")
        elif d_board_bis[ligne][colonne][3]==2 and not king_moved[1] and checked==0:
            if "f8" not in cases_occupied_l and "g8" not in cases_occupied_l and not king_side_rook_moved[1]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "f8" in coup_possiblement_legal(case, [case_actuelle, "g8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "g8" in coup_possiblement_legal(case, [case_actuelle, "g8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "h8" in coup_possiblement_legal(case, [case_actuelle, "g8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="g7" or d_board_bis[ligne_o][colonne_o][0]=="h7":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("g8")
            elif "b8" not in cases_occupied_l and "c8" not in cases_occupied_l and "d8" not in cases_occupied_l and not queen_side_rook_moved[1]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "a8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "b8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "c8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "d8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="a7" or d_board_bis[ligne_o][colonne_o][0]=="b7" or d_board_bis[ligne_o][colonne_o][0]=="c7":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("c8")
    return coups_possibles

def coup_legal(case_actuelle, case_finale, last_move, cases_occupied_l, checked, d_board_l, king_moved, king_side_rook_moved, queen_side_rook_moved):
    coups_possibles = coup_possiblement_legal(case_actuelle, last_move, cases_occupied_l, checked, d_board_l, king_moved, king_side_rook_moved, queen_side_rook_moved)
    ligne = int(case_actuelle[1])-1
    for i in range(8):
        if case_actuelle == d_board_l[ligne][i][0]:
            colonne = i
    if d_board_l[ligne][colonne][1] != "roi":
        for i in range(8):
            for j in range(8):
                if d_board_l[i][j][1]=="roi":
                    if d_board_l[i][j][3]==d_board_l[ligne][colonne][3]:
                        case_roi = d_board_l[i][j][0]
        rejets = []
        for x in range(len(coups_possibles)):
            case_possible = coups_possibles[x]
            prise_roi = 0
            cases_occupied_bis = cases_occupied_l.copy()
            cases_occupied_bis[case_possible]=cases_occupied_l[case_actuelle]
            del cases_occupied_bis[case_actuelle]
            ligne_possible = int(case_possible[1])-1
            for i in range(8):
                if case_possible == d_board_l[ligne_possible][i][0]:
                    colonne_possible = i
            prem = d_board_l[ligne_possible][colonne_possible][1]
            deux = d_board_l[ligne_possible][colonne_possible][2]
            trois = d_board_l[ligne_possible][colonne_possible][3]
            d_board_l[ligne_possible][colonne_possible][1]=d_board_l[ligne][colonne][1]
            d_board_l[ligne_possible][colonne_possible][2]=d_board_l[ligne][colonne][2]
            d_board_l[ligne_possible][colonne_possible][3]=d_board_l[ligne][colonne][3]
            d_board_l[ligne][colonne][1] = None
            d_board_l[ligne][colonne][2] = None
            d_board_l[ligne][colonne][3] = None
            for case in cases_occupied_bis:
                ligne_o = int(case[1])-1
                for p in range(8):
                    if case == d_board_l[ligne_o][p][0]:
                        colonne_o = p
                if d_board_l[ligne_o][colonne_o][3] != d_board_l[ligne][colonne][3]:
                    if case_roi in coup_possiblement_legal(case, [case_actuelle, case_possible, d_board_l[ligne][colonne][3]], cases_occupied_bis, 0, d_board_l, king_moved, king_side_rook_moved, queen_side_rook_moved):
                        prise_roi = 1
            if prise_roi == 1:
                rejets.append(x)
            d_board_l[ligne][colonne][1]=d_board_l[ligne_possible][colonne_possible][1]
            d_board_l[ligne][colonne][2]=d_board_l[ligne_possible][colonne_possible][2]
            d_board_l[ligne][colonne][3]=d_board_l[ligne_possible][colonne_possible][3]
            d_board_l[ligne_possible][colonne_possible][1] = prem
            d_board_l[ligne_possible][colonne_possible][2] = deux
            d_board_l[ligne_possible][colonne_possible][3] = trois
        decal = 0
        for x in rejets:
            del coups_possibles[x-decal]
            decal += 1
    if case_finale in coups_possibles:
        return True
    else:
        return False

def chess_ai(d_board, last_move, cases_occupied_l, checked, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved, turn):
    coups_possibles=[]
    for case in cases_occupied_l:
        ligne = int(case[1])-1
        for i in range(8):
            if case == d_board[ligne][i][0]:
                colonne = i
        if d_board[ligne][colonne][3]==turn:
            coups_potentiels = coup_possiblement_legal(case, last_move, cases_occupied_l, checked, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved)
            for coup in coups_potentiels:
                if coup_legal(case, coup, last_move, cases_occupied_l, checked, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
                    if d_board[ligne][colonne][1]=="pion" and (ligne == 6 or ligne==1):
                        coups_possibles.append([case, coup, "reine"])
                        coups_possibles.append([case, coup, "cavalier"])
                        coups_possibles.append([case, coup, "tour"])
                        coups_possibles.append([case, coup, "fou"])
                    else:
                        coups_possibles.append([case, coup, None])
    nbr_of_moves = len(coups_possibles)
    chosen_move = random.randint(0, nbr_of_moves-1)
    return coups_possibles[chosen_move]