import pygame
from chess_ai_basic import chess_ai
import copy

pygame.init()

board = pygame.display.set_mode((800,600))

#Création de l'échiquier
def board_setup():
    board.fill((229, 231, 233))
    for x in range(8):
        for y in range(8):
            if (x+y)%2==1:
                pygame.draw.rect(board, (81, 90, 90), (x*75, y*75, 75, 75))
    pygame.draw.rect(board, (255,255,255), (600, 0, 200, 600))
    
def end_board_setup():
    board.fill((((229, 231, 233))))

cases = { }

#Je donne les coordonnées x et y ou faut mettre la pièce puis les x et y limites de la case
#Pour les limites: x-gauche, x-droite, y-haut, y-bas
for i in range(8):
    num=i+1
    cases[f"a{num}"] = ((5, 5+(7-i)*75), [0, 75, (8-i)*75, (7-i)*75])

for i in range(8):
    num=i+1
    cases[f"b{num}"] = ((80, 5+(7-i)*75), [75, 150, (8-i)*75, (7-i)*75])

for i in range(8):
    num=i+1
    cases[f"c{num}"] = ((155, 5+(7-i)*75), [150, 225, (8-i)*75, (7-i)*75])

for i in range(8):
    num=i+1
    cases[f"d{num}"] = ((230, 5+(7-i)*75), [225, 300, (8-i)*75, (7-i)*75])

for i in range(8):
    num=i+1
    cases[f"e{num}"] = ((305, 5+(7-i)*75), [300, 375, (8-i)*75, (7-i)*75])

for i in range(8):
    num=i+1
    cases[f"f{num}"] = ((380, 5+(7-i)*75), [375, 450, (8-i)*75, (7-i)*75])

for i in range(8):
    num=i+1
    cases[f"g{num}"] = ((455, 5+(7-i)*75), [450, 525, (8-i)*75, (7-i)*75])

for i in range(8):
    num=i+1
    cases[f"h{num}"] = ((530, 5+(7-i)*75), [525, 600, (8-i)*75, (7-i)*75])
    
#cases_occupied et d_board sont les deux éléments qui traquent ce qui se passe sur l'échiquier. Avoir les deux n'est peut-etre
#optimal en termes de stockage mais me permet de faire moins de calculs.

cases_occupied = {"a1": "tour1_blanc", "a2": "pion1_blanc", "a7": "pion1_noir", "a8": "tour1_noir", "b1": "cavalier1_blanc", "b2": "pion2_blanc", "b7": "pion2_noir", "b8": "cavalier1_noir", "c1": "fou_n_blanc", "c2": "pion3_blanc", "c7": "pion3_noir", "c8": "fou_b_noir", "d1": "reine_blanc", "d2": "pion4_blanc", "d7": "pion4_noir", "d8": "reine_noir", "e1": "roi_blanc", "e2": "pion5_blanc", "e7": "pion5_noir", "e8": "roi_noir", "f1": "fou_b_blanc", "f2": "pion6_blanc", "f7": "pion6_noir", "f8": "fou_n_noir", "g1": "cavalier2_blanc", "g2": "pion7_blanc", "g7": "pion7_noir", "g8": "cavalier2_noir", "h1": "tour2_blanc", "h2": "pion8_blanc", "h7": "pion8_noir", "h8": "tour2_noir"}

#Nom de la case, type de pièce, nom de la pièce
d_board = [[["a1", "tour", "tour1_blanc", 1],["b1", "cavalier", "cava]ier1_blanc", 1],["c1","fou", "fou_n_blanc", 1],["d1", "reine", "reine_blanc", 1],["e1", "roi", "roi_blanc", 1],["f1", "fou", "fou_b_blanc", 1],["g1", "cavalier", "cavalier2_blanc", 1],["h1", "tour", "tour2_blanc", 1]],
         [["a2", "pion", "pion1_blanc", 1],["b2", "pion", "pion2_blanc", 1],["c2", "pion", "pion3_blanc", 1],["d2", "pion", "pion4_blanc", 1],["e2", "pion", "pion5_blanc", 1],["f2", "pion", "pion6_blanc", 1],["g2", "pion", "pion7_blanc", 1], ["h2", "pion", "pion8_blanc", 1]],
         [["a3", None, None, None],["b3", None, None, None],["c3", None, None, None],["d3", None, None, None],["e3", None, None, None],["f3", None, None, None],["g3", None, None, None],["h3", None, None, None]],
         [["a4", None, None, None],["b4", None, None, None],["c4", None, None, None],["d4", None, None, None],["e4", None, None, None],["f4", None, None, None],["g4", None, None, None],["h4", None, None, None]],
         [["a5", None, None, None],["b5", None, None, None],["c5", None, None, None],["d5", None, None, None],["e5", None, None, None],["f5", None, None, None],["g5", None, None, None],["h5", None, None, None]],
         [["a6", None, None, None],["b6", None, None, None],["c6", None, None, None],["d6", None, None, None],["e6", None, None, None],["f6", None, None, None],["g6", None, None, None],["h6", None, None, None]],
         [["a7", "pion", "pion1_noir", 2],["b7", "pion", "pion2_noir", 2],["c7", "pion", "pion3_noir", 2],["d7", "pion", "pion4_noir", 2],["e7", "pion", "pion5_noir", 2],["f7", "pion", "pion6_noir", 2],["g7", "pion", "pion7_noir", 2],["h7", "pion", "pion8_noir", 2]],
         [["a8", "tour", "tour1_blanc", 2],["b8", "cavalier", "cavalier1_noir", 2],["c8", "fou", "fou_b_noir", 2],["d8", "reine", "reine_noir", 2],["e8", "roi", "roi_noir", 2],["f8", "fou", "fou_n_noir", 2],["g8", "cavalier", "cavalier2_noir", 2],["h8", "tour", "tour2_noir", 2]]]

#Fonction pour savoir dans quelle case on est
def case_actuelle(x,y):
    for case in cases.keys():
        if x>cases[case][1][0] and x<cases[case][1][1]:
            if y>cases[case][1][3] and y<cases[case][1][2]:
                return case
    return 0

#Mise en place des pièces et de leurs mouvements sans prendre en compte la légalité des coups

def pion1_blanc(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_blanc.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_blanc.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_blanc.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_blanc.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_blanc.png'), (x,y))

def pion2_blanc(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_blanc.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_blanc.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_blanc.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_blanc.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_blanc.png'), (x,y))

def pion3_blanc(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_blanc.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_blanc.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_blanc.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_blanc.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_blanc.png'), (x,y))

def pion4_blanc(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_blanc.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_blanc.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_blanc.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_blanc.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_blanc.png'), (x,y))

def pion5_blanc(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_blanc.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_blanc.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_blanc.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_blanc.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_blanc.png'), (x,y))

def pion6_blanc(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_blanc.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_blanc.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_blanc.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_blanc.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_blanc.png'), (x,y))

def pion7_blanc(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_blanc.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_blanc.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_blanc.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_blanc.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_blanc.png'), (x,y))

def pion8_blanc(x,y,transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_blanc.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_blanc.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_blanc.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_blanc.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_blanc.png'), (x,y))

def tour1_blanc(x,y):
    board.blit(pygame.image.load('Tour_blanc.png'), (x,y))

def tour2_blanc(x,y):
    board.blit(pygame.image.load('Tour_blanc.png'), (x,y))

def cavalier1_blanc(x,y):
    board.blit(pygame.image.load('Cavalier_blanc.png'), (x,y))

def cavalier2_blanc(x,y):
    board.blit(pygame.image.load('Cavalier_blanc.png'), (x,y))

def fou_n_blanc(x,y):
    board.blit(pygame.image.load('Fou_blanc.png'), (x,y))

def fou_b_blanc(x,y):
    board.blit(pygame.image.load('Fou_blanc.png'), (x,y))

def reine_blanc(x,y):
    board.blit(pygame.image.load('Reine_blanc.png'), (x,y))

def roi_blanc(x,y):
    board.blit(pygame.image.load('Roi_blanc.png'), (x,y))

def pion1_noir(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_noir.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_noir.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_noir.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_noir.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_noir.png'), (x,y))

def pion2_noir(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_noir.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_noir.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_noir.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_noir.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_noir.png'), (x,y))

def pion3_noir(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_noir.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_noir.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_noir.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_noir.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_noir.png'), (x,y))

def pion4_noir(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_noir.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_noir.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_noir.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_noir.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_noir.png'), (x,y))

def pion5_noir(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_noir.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_noir.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_noir.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_noir.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_noir.png'), (x,y))

def pion6_noir(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_noir.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_noir.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_noir.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_noir.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_noir.png'), (x,y))

def pion7_noir(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_noir.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_noir.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_noir.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_noir.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_noir.png'), (x,y))

def pion8_noir(x,y, transfo):
    if transfo==None:
        board.blit(pygame.image.load('Pion_noir.png'), (x,y))
    elif transfo=="reine":
        board.blit(pygame.image.load('Reine_noir.png'), (x,y))
    elif transfo=="cavalier":
        board.blit(pygame.image.load('Cavalier_noir.png'), (x,y))
    elif transfo=="tour":
        board.blit(pygame.image.load('Tour_noir.png'), (x,y))
    elif transfo=="fou":
        board.blit(pygame.image.load('Fou_noir.png'), (x,y))

def tour1_noir(x,y):
    board.blit(pygame.image.load('Tour_noir.png'), (x,y))

def tour2_noir(x,y):
    board.blit(pygame.image.load('Tour_noir.png'), (x,y))

def cavalier1_noir(x,y):
    board.blit(pygame.image.load('Cavalier_noir.png'), (x,y))

def cavalier2_noir(x,y):
    board.blit(pygame.image.load('Cavalier_noir.png'), (x,y))

def fou_b_noir(x,y):
    board.blit(pygame.image.load('Fou_noir.png'), (x,y))

def fou_n_noir(x,y):
    board.blit(pygame.image.load('Fou_noir.png'), (x,y))

def reine_noir(x,y):
    board.blit(pygame.image.load('Reine_noir.png'), (x,y))

def roi_noir(x,y):
    board.blit(pygame.image.load('Roi_noir.png'), (x,y))
    
my_piece_functions = {"tour1_blanc": tour1_blanc, "pion1_blanc": pion1_blanc, "pion1_noir": pion1_noir, "tour1_noir": tour1_noir, "cavalier1_blanc": cavalier1_blanc, "pion2_blanc": pion2_blanc, "pion2_noir": pion2_noir, "cavalier1_noir": cavalier1_noir, "fou_n_blanc": fou_n_blanc, "pion3_blanc": pion3_blanc, "pion3_noir": pion3_noir, "fou_b_noir": fou_b_noir, "reine_blanc": reine_blanc, "pion4_blanc": pion4_blanc, "pion4_noir": pion4_noir, "reine_noir": reine_noir, "roi_blanc": roi_blanc, "pion5_blanc": pion5_blanc, "pion5_noir": pion5_noir, "roi_noir": roi_noir, "fou_b_blanc": fou_b_blanc, "pion6_blanc": pion6_blanc, "pion6_noir": pion6_noir, "fou_n_noir": fou_n_noir, "cavalier2_blanc": cavalier2_blanc, "pion7_blanc": pion7_blanc, "pion7_noir": pion7_noir, "cavalier2_noir": cavalier2_noir, "tour2_blanc": tour2_blanc, "pion8_blanc": pion8_blanc, "pion8_noir": pion8_noir, "tour2_noir": tour2_noir}

#Initialization des positions des pièces
pion1_blanc_x = cases["a2"][0][0]
pion1_blanc_y = cases["a2"][0][1]
pion1_blanc_transfo = None
pion2_blanc_x = cases["b2"][0][0]
pion2_blanc_y = cases["b2"][0][1]
pion2_blanc_transfo = None
pion3_blanc_x = cases["c2"][0][0]
pion3_blanc_y = cases["c2"][0][1]
pion3_blanc_transfo = None
pion4_blanc_x = cases["d2"][0][0]
pion4_blanc_y = cases["d2"][0][1]
pion4_blanc_transfo = None
pion5_blanc_x = cases["e2"][0][0]
pion5_blanc_y = cases["e2"][0][1]
pion5_blanc_transfo = None
pion6_blanc_x = cases["f2"][0][0]
pion6_blanc_y = cases["f2"][0][1]
pion6_blanc_transfo = None
pion7_blanc_x = cases["g2"][0][0]
pion7_blanc_y = cases["g2"][0][1]
pion7_blanc_transfo = None
pion8_blanc_x = cases["h2"][0][0]
pion8_blanc_y = cases["h2"][0][1]
pion8_blanc_transfo=None
pion1_noir_x = cases["a7"][0][0]
pion1_noir_y = cases["a7"][0][1]
pion1_noir_transfo = None
pion2_noir_x = cases["b7"][0][0]
pion2_noir_y = cases["b7"][0][1]
pion2_noir_transfo = None
pion3_noir_x = cases["c7"][0][0]
pion3_noir_y = cases["c7"][0][1]
pion3_noir_transfo = None
pion4_noir_x = cases["d7"][0][0]
pion4_noir_y = cases["d7"][0][1]
pion4_noir_transfo = None
pion5_noir_x = cases["e7"][0][0]
pion5_noir_y = cases["e7"][0][1]
pion5_noir_transfo = None
pion6_noir_x = cases["f7"][0][0]
pion6_noir_y = cases["f7"][0][1]
pion6_noir_transfo = None
pion7_noir_x = cases["g7"][0][0]
pion7_noir_y = cases["g7"][0][1]
pion7_noir_transfo = None
pion8_noir_x = cases["h7"][0][0]
pion8_noir_y = cases["h7"][0][1]
pion8_noir_transfo = None
tour1_blanc_x = cases["a1"][0][0]
tour1_blanc_y = cases["a1"][0][1]
tour2_blanc_x = cases["h1"][0][0]
tour2_blanc_y = cases["h1"][0][1]
cavalier1_blanc_x = cases["b1"][0][0]
cavalier1_blanc_y = cases["b1"][0][1]
cavalier2_blanc_x = cases["g1"][0][0]
cavalier2_blanc_y = cases["g1"][0][1]
fou_n_blanc_x = cases["c1"][0][0]
fou_n_blanc_y = cases["c1"][0][1]
fou_b_blanc_x = cases["f1"][0][0]
fou_b_blanc_y = cases["f1"][0][1]
reine_blanc_x = cases["d1"][0][0]
reine_blanc_y = cases["d1"][0][1]
roi_blanc_x = cases["e1"][0][0]
roi_blanc_y = cases["e1"][0][1]
tour1_noir_x = cases["a8"][0][0]
tour1_noir_y = cases["a8"][0][1]
tour2_noir_x = cases["h8"][0][0]
tour2_noir_y = cases["h8"][0][1]
cavalier1_noir_x = cases["b8"][0][0]
cavalier1_noir_y = cases["b8"][0][1]
cavalier2_noir_x = cases["g8"][0][0]
cavalier2_noir_y = cases["g8"][0][1]
fou_n_noir_x = cases["f8"][0][0]
fou_n_noir_y = cases["f8"][0][1]
fou_b_noir_x = cases["c8"][0][0]
fou_b_noir_y = cases["c8"][0][1]
reine_noir_x = cases["d8"][0][0]
reine_noir_y = cases["d8"][0][1]
roi_noir_x = cases["e8"][0][0]
roi_noir_y = cases["e8"][0][1]

#Fonction qui rend les coups légaux possibles pour une pièce donnée à partir d'une case donnée sans prendre en compte certaines choses comme les clouages
def coup_possiblement_legal(case_actuelle, last_move, cases_occupied_l, checked, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
    ligne = int(case_actuelle[1])-1
    coups_possibles = []
    for i in range(8):
        if case_actuelle == d_board_bis[ligne][i][0]:
            colonne = i
    if d_board_bis[ligne][colonne][1] == "tour" or d_board_bis[ligne][colonne][1] == "reine":
        i=colonne + 1
        while i<8:
            if d_board_bis[ligne][i][1] == None:
                coups_possibles.append(d_board_bis[ligne][i][0])
                i+=1
            else:
                if d_board_bis[ligne][i][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[ligne][i][0])
                i=8
        i = colonne -1
        while i>=0:
            if d_board_bis[ligne][i][1] == None:
                coups_possibles.append(d_board_bis[ligne][i][0])
                i-=1
            else:
                if d_board_bis[ligne][i][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[ligne][i][0])
                i=-1
        j = ligne + 1
        while j<8:
            if d_board_bis[j][colonne][1] == None:
                coups_possibles.append(d_board_bis[j][colonne][0])
                j+=1
            else:
                if d_board_bis[j][colonne][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[j][colonne][0])
                j=8
        j = ligne -1
        while j>=0:
            if d_board_bis[j][colonne][1] == None:
                coups_possibles.append(d_board_bis[j][colonne][0])
                j-=1
            else:
                if d_board_bis[j][colonne][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[j][colonne][0])
                j=-1
    elif d_board_bis[ligne][colonne][1] == "cavalier":
        col_a = colonne + 2
        col_b = colonne + 1
        col_c = colonne - 1
        col_d = colonne - 2
        lig_a = ligne + 2
        lig_b = ligne + 1
        lig_c = ligne - 1
        lig_d = ligne - 2
        if col_a < 8:
            if lig_b < 8:
                if d_board_bis[lig_b][col_a][1] == None or d_board_bis[lig_b][col_a][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board[lig_b][col_a][0])
            if lig_c >= 0:
                if d_board_bis[lig_c][col_a][1] == None or d_board_bis[lig_c][col_a][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_c][col_a][0]) 
        if col_d >= 0:
            if lig_b < 8:
                if d_board_bis[lig_b][col_d][1] == None or d_board_bis[lig_b][col_d][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_b][col_d][0])
            if lig_c >= 0:
                if d_board_bis[lig_c][col_d][1] == None or d_board_bis[lig_c][col_d][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_c][col_d][0])
        if col_b < 8:
            if lig_a < 8:
                if d_board_bis[lig_a][col_b][1] == None or d_board_bis[lig_a][col_b][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_a][col_b][0])
            if lig_d >= 0:
                if d_board_bis[lig_d][col_b][1] == None or d_board_bis[lig_d][col_b][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_d][col_b][0]) 
        if col_c >= 0:
            if lig_a < 8:
                if d_board_bis[lig_a][col_c][1] == None or d_board_bis[lig_a][col_c][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_a][col_c][0])
            if lig_d >= 0:
                if d_board_bis[lig_d][col_c][1] == None or d_board_bis[lig_d][col_c][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_d][col_c][0])
    if d_board_bis[ligne][colonne][1] == "fou" or d_board_bis[ligne][colonne][1] == "reine":
        a=ligne+1
        b=colonne+1
        while a<8 and b<8:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a+=1
                b+=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=8
                b=8
        a=ligne+1
        b=colonne-1
        while a<8 and b>=0:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a+=1
                b-=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=8
                b=-1
        a=ligne-1
        b=colonne-1
        while a>=0 and b>=0:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a-=1
                b-=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=-1
                b=-1
        a=ligne-1
        b=colonne+1
        while a>=0 and b<8:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a-=1
                b+=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=-1
                b=8
    elif d_board_bis[ligne][colonne][1] == "pion":
        if d_board_bis[ligne][colonne][3] == 1:
            if ligne<7:
                if ligne == 1:
                    if d_board_bis[ligne+1][colonne][1] == None and d_board_bis[ligne+1][colonne][1] == None:
                        coups_possibles.append(d_board_bis[ligne+2][colonne][0])
                elif ligne == 4:
                    if colonne + 1 <8:
                        if last_move[0]==d_board_bis[ligne+2][colonne+1][0] and last_move[1]==d_board_bis[ligne][colonne+1][0] and last_move[2]=="pion":
                            coups_possibles.append(d_board_bis[ligne+1][colonne+1][0])
                    if colonne - 1 >=0:
                        if last_move[0]==d_board_bis[ligne+2][colonne-1][0] and last_move[1]==d_board_bis[ligne][colonne-1][0] and last_move[2]=="pion":
                            coups_possibles.append(d_board_bis[ligne+1][colonne-1][0])
                if d_board_bis[ligne+1][colonne][1] == None:
                    coups_possibles.append(d_board_bis[ligne+1][colonne][0])
                if colonne+1<8:
                    if d_board_bis[ligne+1][colonne+1][3] == 2:
                        coups_possibles.append(d_board_bis[ligne+1][colonne+1][0])
                if colonne-1>=0:
                    if d_board_bis[ligne+1][colonne-1][3] == 2:
                        coups_possibles.append(d_board_bis[ligne+1][colonne-1][0])
        elif d_board_bis[ligne][colonne][3] == 2:
            if ligne == 6:
                if d_board_bis[ligne-1][colonne][1] == None and d_board_bis[ligne-2][colonne][1] == None:
                    coups_possibles.append(d_board_bis[ligne-2][colonne][0])
            elif ligne == 3:
                if colonne + 1 <8:
                    if last_move[0]==d_board_bis[ligne-2][colonne+1][0] and last_move[1]==d_board_bis[ligne][colonne+1][0] and last_move[2]=="pion":
                        coups_possibles.append(d_board_bis[ligne-1][colonne+1][0])
                if colonne - 1 >=0:
                    if last_move[0]==d_board_bis[ligne-2][colonne-1][0] and last_move[1]==d_board_bis[ligne][colonne-1][0] and last_move[2]=="pion":
                        coups_possibles.append(d_board_bis[ligne-1][colonne-1][0])
            if d_board_bis[ligne-1][colonne][1] == None:
                coups_possibles.append(d_board_bis[ligne-1][colonne][0])
            if colonne+1<8:
                if d_board_bis[ligne-1][colonne+1][3] == 1:
                    coups_possibles.append(d_board_bis[ligne-1][colonne+1][0])
            if colonne-1>=0:
                if d_board_bis[ligne-1][colonne-1][3] == 1:
                    coups_possibles.append(d_board_bis[ligne-1][colonne-1][0])
    if d_board_bis[ligne][colonne][1] == "roi":
        for i in range(-1,2):
            for j in range(-1,2):
                if i!=0 or j!=0:
                    if ligne+i>=0 and ligne+i<8 and colonne+j>=0 and colonne+j<8:
                        if d_board_bis[ligne+i][colonne+j][3]==None or d_board_bis[ligne+i][colonne+j][3]!=d_board_bis[ligne][colonne][3]:
                            case_possible = d_board_bis[ligne+i][colonne+j][0]
                            if last_move != None:
                                if last_move[2]!="roi":
                                    prise_roi = 0
                                    for case in cases_occupied_l:
                                        ligne_o = int(case[1])-1
                                        for p in range(8):
                                            if case == d_board_bis[ligne_o][p][0]:
                                                colonne_o = p
                                        if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                                            cases_occupied_bis = cases_occupied_l.copy()
                                            cases_occupied_bis[case_possible]=cases_occupied_l[case_actuelle]
                                            del cases_occupied_bis[case_actuelle]
                                            ligne_possible = ligne+i
                                            colonne_possible = colonne+j
                                            prem = d_board_bis[ligne_possible][colonne_possible][1]
                                            deux = d_board_bis[ligne_possible][colonne_possible][2]
                                            trois = d_board_bis[ligne_possible][colonne_possible][3]
                                            d_board_bis[ligne_possible][colonne_possible][1]=d_board_bis[ligne][colonne][1]
                                            d_board_bis[ligne_possible][colonne_possible][2]=d_board_bis[ligne][colonne][2]
                                            d_board_bis[ligne_possible][colonne_possible][3]=d_board_bis[ligne][colonne][3]
                                            d_board_bis[ligne][colonne][1] = None
                                            d_board_bis[ligne][colonne][2] = None
                                            d_board_bis[ligne][colonne][3] = None
                                            if case_possible in coup_possiblement_legal(case, [case_actuelle, case_possible, "roi"], cases_occupied_bis, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                                prise_roi = 1
                                            d_board_bis[ligne][colonne][1]=d_board_bis[ligne_possible][colonne_possible][1]
                                            d_board_bis[ligne][colonne][2]=d_board_bis[ligne_possible][colonne_possible][2]
                                            d_board_bis[ligne][colonne][3]=d_board_bis[ligne_possible][colonne_possible][3]
                                            d_board_bis[ligne_possible][colonne_possible][1] = prem
                                            d_board_bis[ligne_possible][colonne_possible][2] = deux
                                            d_board_bis[ligne_possible][colonne_possible][3] = trois
                                    if prise_roi == 0:
                                        coups_possibles.append(d_board_bis[ligne+i][colonne+j][0])
                                else:
                                    coups_possibles.append(d_board_bis[ligne+i][colonne+j][0])
        if d_board_bis[ligne][colonne][3]==1 and not king_moved[0] and checked==0:
            if "f1" not in cases_occupied_l and "g1" not in cases_occupied_l and not king_side_rook_moved[0]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "f1" in coup_possiblement_legal(case, [case_actuelle, "g1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "g1" in coup_possiblement_legal(case, [case_actuelle, "g1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "h1" in coup_possiblement_legal(case, [case_actuelle, "g1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="g2" or d_board_bis[ligne_o][colonne_o][0]=="h2":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("g1")
            elif "b1" not in cases_occupied_l and "c1" not in cases_occupied_l and "d1" not in cases_occupied_l and not queen_side_rook_moved[0]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "a1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "b1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "c1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "d1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="a2" or d_board_bis[ligne_o][colonne_o][0]=="b2" or d_board_bis[ligne_o][colonne_o][0]=="c2":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("c1")
        elif d_board_bis[ligne][colonne][3]==2 and not king_moved[1] and checked==0:
            if "f8" not in cases_occupied_l and "g8" not in cases_occupied_l and not king_side_rook_moved[1]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "f8" in coup_possiblement_legal(case, [case_actuelle, "g8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "g8" in coup_possiblement_legal(case, [case_actuelle, "g8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "h8" in coup_possiblement_legal(case, [case_actuelle, "g8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="g7" or d_board_bis[ligne_o][colonne_o][0]=="h7":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("g8")
            elif "b8" not in cases_occupied_l and "c8" not in cases_occupied_l and "d8" not in cases_occupied_l and not queen_side_rook_moved[1]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "a8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "b8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "c8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "d8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="a7" or d_board_bis[ligne_o][colonne_o][0]=="b7" or d_board_bis[ligne_o][colonne_o][0]=="c7":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("c8")
    return coups_possibles

#Fonction qui nous dit si un coup est légal ou non
def coup_legal(case_actuelle, case_finale, last_move, cases_occupied_l, checked, d_board_l):
    coups_possibles = coup_possiblement_legal(case_actuelle, last_move, cases_occupied_l, checked, d_board_l, king_moved, king_side_rook_moved, queen_side_rook_moved)
    ligne = int(case_actuelle[1])-1
    for i in range(8):
        if case_actuelle == d_board_l[ligne][i][0]:
            colonne = i
    if d_board_l[ligne][colonne][1] != "roi":
        for i in range(8):
            for j in range(8):
                if d_board_l[i][j][1]=="roi":
                    if d_board_l[i][j][3]==d_board_l[ligne][colonne][3]:
                        case_roi = d_board_l[i][j][0]
        rejets = []
        for x in range(len(coups_possibles)):
            case_possible = coups_possibles[x]
            prise_roi = 0
            cases_occupied_bis = cases_occupied_l.copy()
            cases_occupied_bis[case_possible]=cases_occupied_l[case_actuelle]
            del cases_occupied_bis[case_actuelle]
            ligne_possible = int(case_possible[1])-1
            for i in range(8):
                if case_possible == d_board_l[ligne_possible][i][0]:
                    colonne_possible = i
            prem = d_board_l[ligne_possible][colonne_possible][1]
            deux = d_board_l[ligne_possible][colonne_possible][2]
            trois = d_board_l[ligne_possible][colonne_possible][3]
            d_board_l[ligne_possible][colonne_possible][1]=d_board_l[ligne][colonne][1]
            d_board_l[ligne_possible][colonne_possible][2]=d_board_l[ligne][colonne][2]
            d_board_l[ligne_possible][colonne_possible][3]=d_board_l[ligne][colonne][3]
            d_board_l[ligne][colonne][1] = None
            d_board_l[ligne][colonne][2] = None
            d_board_l[ligne][colonne][3] = None
            for case in cases_occupied_bis:
                ligne_o = int(case[1])-1
                for p in range(8):
                    if case == d_board_l[ligne_o][p][0]:
                        colonne_o = p
                if d_board_l[ligne_o][colonne_o][3] != d_board_l[ligne][colonne][3]:
                    if case_roi in coup_possiblement_legal(case, [case_actuelle, case_possible, d_board_l[ligne][colonne][3]], cases_occupied_bis, 0, d_board_l, king_moved, king_side_rook_moved, queen_side_rook_moved):
                        prise_roi = 1
            if prise_roi == 1:
                rejets.append(x)
            d_board_l[ligne][colonne][1]=d_board_l[ligne_possible][colonne_possible][1]
            d_board_l[ligne][colonne][2]=d_board_l[ligne_possible][colonne_possible][2]
            d_board_l[ligne][colonne][3]=d_board_l[ligne_possible][colonne_possible][3]
            d_board_l[ligne_possible][colonne_possible][1] = prem
            d_board_l[ligne_possible][colonne_possible][2] = deux
            d_board_l[ligne_possible][colonne_possible][3] = trois
        decal = 0
        for x in rejets:
            del coups_possibles[x-decal]
            decal += 1
    if case_finale in coups_possibles:
        return True
    else:
        return False

#Vérification d'échec et mat
def checkmate(case_actuelle, cases_occupied_l, last_move):
    ligne = int(case_actuelle[1])-1
    for i in range(8):
        if case_actuelle == d_board[ligne][i][0]:
            colonne = i
    for case in cases_occupied_l:
        ligne_c = int(case[1])-1
        for i in range(8):
            if case == d_board[ligne_c][i][0]:
                colonne_c = i
        if d_board[ligne_c][colonne_c][3]==d_board[ligne][colonne][3]:
            for move in coup_possiblement_legal(case, last_move, cases_occupied_l, 1, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
                if coup_legal(case, move, last_move, cases_occupied_l, 1, d_board):
                    return False
    return True
 
 #Vérification de pat               
def stalemate(cases_occupied_l, who_turn):
    if who_turn==1:
        who_turn = 2
    else:
        who_turn = 1
    for case in cases_occupied_l:
        ligne = int(case[1])-1
        for i in range(8):
            if case == d_board[ligne][i][0]:
                colonne = i
        if d_board[ligne][colonne][3]==who_turn:
            for move in coup_possiblement_legal(case, last_move, cases_occupied_l, 0, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
                if coup_legal(case, move, last_move, cases_occupied_l, 0, d_board):
                    return False
    return True

#Vérification de nul
def draw(positions, d_board_l, cases_occupied_l):
    for position in positions:
        if position[0]==d_board_l:
            position[1] += 1
            if position[1]==3:
                return True
    if len(cases_occupied_l)<=5:
        white_pieces = []
        black_pieces = []
        for case in cases_occupied_l:
            ligne_c = int(case[1])-1
            for j in range(8):
                if case == d_board_l[ligne_c][j][0]:
                    colonne_c = j
            if ligne_c == 1:
                white_pieces.append(d_board_l[ligne_c][colonne_c][1])
            else:
                black_pieces.append(d_board_l[ligne_c][colonne_c][1])
        if "pion" not in white_pieces and "tour" not in white_pieces and "reine" not in white_pieces and "pion" not in black_pieces and "tour" not in black_pieces and "reine" not in black_pieces:
            if len(white_pieces)==3:
                if "fou" not in black_pieces:
                    cavalier_count=0
                    for piece in white_pieces:
                        if piece == "cavalier":
                            cavalier_count += 1
                    if cavalier_count == 2:
                        return True
            elif len(black_pieces)==3:
                if "fou" not in white_pieces:
                    cavalier_count=0
                    for piece in black_pieces:
                        if piece == "cavalier":
                            cavalier_count += 1
                    if cavalier_count == 2:
                        return True
            elif len(cases_occupied_l)<=4:
                return True
    return False

#Logo and caption
pygame.display.set_caption("Chess")
icon = pygame.image.load('jeu-dechecs-2.png')
pygame.display.set_icon(icon)

color_active = pygame.Color('black') 
color_passive = pygame.Color('white') 
color = color_active
base_font = pygame.font.Font(None, 32) 
text_white_win = 'WHITE WINS!'
text_black_win = "BLACK WINS!"
text_stalemate = "STALEMATE"
text_draw = "DRAW"
text = " "

class Button:
    def __init__(self, text, x, y, width, height, color, text_color):
        self.rect = pygame.Rect(x, y, width, height)
        self.text = text
        self.color = color
        self.text_color = text_color

    def draw(self, board):
        pygame.draw.rect(board, self.color, self.rect)
        font = pygame.font.Font(None, 36)
        text_surface = font.render(self.text, True, self.text_color)
        text_rect = text_surface.get_rect()
        text_rect.center = self.rect.center
        board.blit(text_surface, text_rect)
        
color_prom_buttons = color_passive
button_reine = Button("Reine", 600, 100, 100, 50, color_active, color_passive)
button_cavalier = Button("Cavalier", 600, 150, 100, 50, color_active, color_passive)
button_tour = Button("Tour", 600, 200, 100, 50, color_active, color_passive)
button_fou = Button("Fou", 600, 250, 100, 50, color_active, color_passive)
button_white = Button("White", 600, 250, 200, 100, color_active, color_passive)
button_black = Button("Black", 600, 350, 200, 100, color_active, color_passive)

#Le jeu
positions=[]
selected_button = None
turn = 1
running = True
clicked = None
last_move = None
check = 0
king_moved=[False, False]
king_side_rook_moved=[False, False]
queen_side_rook_moved=[False, False]
sides = None
end = False
screen = "game"
while running:
    if screen=="game":
        board_setup()
        pion1_blanc(pion1_blanc_x, pion1_blanc_y, pion1_blanc_transfo)
        pion2_blanc(pion2_blanc_x, pion2_blanc_y, pion2_blanc_transfo)
        pion3_blanc(pion3_blanc_x, pion3_blanc_y, pion3_blanc_transfo)
        pion4_blanc(pion4_blanc_x, pion4_blanc_y, pion4_blanc_transfo)
        pion5_blanc(pion5_blanc_x, pion5_blanc_y, pion5_blanc_transfo)
        pion6_blanc(pion6_blanc_x, pion6_blanc_y, pion6_blanc_transfo)
        pion7_blanc(pion7_blanc_x, pion7_blanc_y, pion7_blanc_transfo)
        pion8_blanc(pion8_blanc_x, pion8_blanc_y, pion8_blanc_transfo)
        tour1_blanc(tour1_blanc_x, tour1_blanc_y)
        tour2_blanc(tour2_blanc_x, tour2_blanc_y)
        cavalier1_blanc(cavalier1_blanc_x, cavalier1_blanc_y)
        cavalier2_blanc(cavalier2_blanc_x, cavalier2_blanc_y)
        fou_b_blanc(fou_b_blanc_x, fou_b_blanc_y)
        fou_n_blanc(fou_n_blanc_x, fou_n_blanc_y)
        reine_blanc(reine_blanc_x, reine_blanc_y)
        roi_blanc(roi_blanc_x, roi_blanc_y)
        pion1_noir(pion1_noir_x, pion1_noir_y, pion1_noir_transfo)
        pion2_noir(pion2_noir_x, pion2_noir_y, pion2_noir_transfo)
        pion3_noir(pion3_noir_x, pion3_noir_y, pion3_noir_transfo)
        pion4_noir(pion4_noir_x, pion4_noir_y, pion4_noir_transfo)
        pion5_noir(pion5_noir_x, pion5_noir_y, pion5_noir_transfo)
        pion6_noir(pion6_noir_x, pion6_noir_y, pion6_noir_transfo)
        pion7_noir(pion7_noir_x, pion7_noir_y, pion7_noir_transfo)
        pion8_noir(pion8_noir_x, pion8_noir_y, pion8_noir_transfo)
        tour1_noir(tour1_noir_x, tour1_noir_y)
        tour2_noir(tour2_noir_x, tour2_noir_y)
        cavalier1_noir(cavalier1_noir_x, cavalier1_noir_y)
        cavalier2_noir(cavalier2_noir_x, cavalier2_noir_y)
        fou_b_noir(fou_b_noir_x, fou_b_noir_y)
        fou_n_noir(fou_n_noir_x, fou_n_noir_y)
        reine_noir(reine_noir_x, reine_noir_y)
        roi_noir(roi_noir_x, roi_noir_y)
        while sides==None:
            button_white.draw(board)
            button_black.draw(board)
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        for button in [button_white, button_black]:
                            if button.rect.collidepoint(event.pos):
                                if button == button_white:
                                    sides = {1: "human", 2: "CPU"}
                                else:
                                    sides = {1: "CPU", 2: "human"}
            pygame.display.update()
        
        if end:
            pygame.time.delay(1000)
            running=False

        if sides[turn] == "human":
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        for case in cases_occupied:
                            if case == case_actuelle(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1]):
                                clicked = case

                if event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1:
                        if clicked != None:
                            case_finale = case_actuelle(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])
                            ligne = int(clicked[1])-1
                            ligne_f = int(case_finale[1])-1
                            for j in range(8):
                                if clicked == d_board[ligne][j][0]:
                                    colonne = j
                            for j in range(8):
                                if case_finale == d_board[ligne_f][j][0]:
                                    colonne_f = j
                            for i in range(8):
                                if d_board[ligne][i][0]==clicked:
                                    colonne=i
                                    if d_board[ligne][i][3]==turn and coup_legal(clicked, case_finale, last_move, cases_occupied, check, d_board):
                                        if case_finale in cases_occupied:
                                            if d_board[ligne_f][colonne_f][3] == 0:
                                                final_y = 150
                                            else:
                                                final_y = 450
                                            new_pos_x = cases_occupied[case_finale] + "_x"
                                            new_pos_y = cases_occupied[case_finale] + "_y"
                                            exec("%s = %d" % (new_pos_x, 700))
                                            exec("%s = %d" % (new_pos_y, final_y))
                                            del cases_occupied[case_finale]
                                        elif d_board[ligne][colonne][1]=="pion":
                                            if ligne_f == ligne+1 and colonne_f == colonne+1:
                                                en_passant = d_board[ligne][colonne_f][0]
                                                if d_board[ligne][colonne_f][3] == 0:
                                                    final_y = 150
                                                else:
                                                    final_y = 450
                                                new_pos_x = cases_occupied[en_passant] + "_x"
                                                new_pos_y = cases_occupied[en_passant] + "_y"
                                                exec("%s = %d" % (new_pos_x, 700))
                                                exec("%s = %d" % (new_pos_y, final_y))
                                                del cases_occupied[en_passant]
                                                d_board[ligne][colonne_f][1]=None
                                                d_board[ligne][colonne_f][2]=None
                                                d_board[ligne][colonne_f][3]=None
                                        elif d_board[ligne][colonne][1]=="roi" and d_board[ligne][colonne][0]=="e1" and d_board[ligne_f][colonne_f][0]=="g1":
                                            tour2_blanc_x = cases["f1"][0][0] 
                                            tour2_blanc_y = cases["f1"][0][1]
                                            d_board[0][7][1] = None
                                            d_board[0][7][2] = None
                                            d_board[0][7][3] = None
                                            d_board[0][5][1] = "tour"
                                            d_board[0][5][2] = "tour2_blanc"
                                            d_board[0][5][3] = 1
                                            del cases_occupied["h1"]
                                            cases_occupied["f1"]="tour2_blanc"
                                        elif d_board[ligne][colonne][1]=="roi" and d_board[ligne][colonne][0]=="e1" and d_board[ligne_f][colonne_f][0]=="c1":
                                            tour1_blanc_x = cases["d1"][0][0]
                                            tour1_blanc_y = cases["d1"][0][1]
                                            d_board[0][0][1] = None
                                            d_board[0][0][2] = None
                                            d_board[0][0][3] = None
                                            d_board[0][3][1] = "tour"
                                            d_board[0][3][2] = "tour1_blanc"
                                            d_board[0][3][3] = 1
                                            del cases_occupied["a1"]
                                            cases_occupied["d1"]="tour1_blanc"
                                        elif d_board[ligne][colonne][1]=="roi" and d_board[ligne][colonne][0]=="e8" and d_board[ligne_f][colonne_f][0]=="g8":
                                            tour2_noir_x = cases["f8"][0][0]
                                            tour2_noir_y = cases["f8"][0][1]
                                            d_board[7][7][1] = None
                                            d_board[7][7][2] = None
                                            d_board[7][7][3] = None
                                            d_board[7][5][1] = "tour"
                                            d_board[7][5][2] = "tour2_noir"
                                            d_board[7][5][3] = 2
                                            del cases_occupied["h8"]
                                            cases_occupied["f8"]="tour2_noir"
                                        elif d_board[ligne][colonne][1]=="roi" and d_board[ligne][colonne][0]=="e8" and d_board[ligne_f][colonne_f][0]=="c8":
                                            tour1_noir_x = cases["d8"][0][0]
                                            tour1_noir_y = cases["d8"][0][1]
                                            d_board[7][0][1] = None
                                            d_board[7][0][2] = None
                                            d_board[7][0][3] = None
                                            d_board[7][3][1] = "tour"
                                            d_board[7][3][2] = "tour1_noir"
                                            d_board[7][3][3] = 2
                                            del cases_occupied["a8"]
                                            cases_occupied["d8"]="tour1_noir"
                                        if d_board[ligne][colonne][1]=="pion" and (ligne_f==7 or ligne_f==0):
                                            while selected_button==None:
                                                button_reine.draw(board)
                                                button_cavalier.draw(board)
                                                button_tour.draw(board)
                                                button_fou.draw(board)
                                                for event in pygame.event.get():
                                                    if event.type == pygame.MOUSEBUTTONDOWN:
                                                        if event.button == 1:
                                                            for button in [button_reine, button_cavalier, button_tour, button_fou]:
                                                                if button.rect.collidepoint(event.pos):
                                                                    selected_button = button
                                                pygame.display.update()
                                            if selected_button == button_reine:
                                                promotion = "reine"
                                            elif selected_button == button_cavalier:
                                                promotion = "cavalier"
                                            elif selected_button == button_tour:
                                                promotion = "tour"
                                            elif selected_button == button_fou:
                                                promotion = "fou"
                                            new_transfo = d_board[ligne][colonne][2] + "_transfo"
                                            print(new_transfo)
                                            exec(f"{new_transfo} = '{promotion}'")
                                            print(pion8_blanc_transfo)
                                            d_board[ligne][colonne][1]=promotion
                                            selected_button=None
                                        new_pos_x = cases_occupied[clicked] + "_x"
                                        new_pos_y = cases_occupied[clicked] + "_y"
                                        exec("%s = %d" % (new_pos_x, cases[case_finale][0][0]))
                                        exec("%s = %d" % (new_pos_y, cases[case_finale][0][1]))
                                        if d_board[ligne][colonne][2]=="roi_blanc":
                                            king_moved[0] = True
                                        elif d_board[ligne][colonne][2]=="roi_noir":
                                            king_moved[1] = True
                                        elif d_board[ligne][colonne][2]=="tour1_blanc":
                                            queen_side_rook_moved[0] = True
                                        elif d_board[ligne][colonne][2]=="tour1_noir":
                                            queen_side_rook_moved[1] = True
                                        elif d_board[ligne][colonne][2]=="tour2_blanc":
                                            king_side_rook_moved[0] = True
                                        elif d_board[ligne][colonne][2]=="tour2_noir":
                                            king_side_rook_moved[1] = True
                                        cases_occupied[case_finale]=cases_occupied[clicked]
                                        del cases_occupied[clicked]
                                        last_move = [clicked, case_finale, d_board[ligne][colonne][1]]
                                        d_board[ligne_f][colonne_f][1]=d_board[ligne][i][1]
                                        d_board[ligne_f][colonne_f][2]=d_board[ligne][i][2]
                                        d_board[ligne_f][colonne_f][3]=d_board[ligne][i][3]
                                        d_board[ligne][i][1] = None
                                        d_board[ligne][i][2] = None
                                        d_board[ligne][i][3] = None
                                        positions.append(d_board[:])
                                        check = 0
                                        if turn ==1:
                                            for case in cases_occupied:
                                                if cases_occupied[case]=="roi_noir":
                                                    case_roi = case
                                        if turn ==2:
                                            for case in cases_occupied:
                                                if cases_occupied[case]=="roi_blanc":
                                                    case_roi = case
                                        for case in cases_occupied:
                                            if case_roi in coup_possiblement_legal(case, last_move, cases_occupied, check, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                                check=1
                                        if check == 1:
                                            if checkmate(case_roi, cases_occupied, last_move):
                                                if turn==1:
                                                    screen = "end"
                                                    text = text_white_win
                                                else:
                                                    screen = "end"
                                                    text = text_black_win
                                                end = True 
                                        else:
                                            if stalemate(cases_occupied, turn):
                                                screen = "end"
                                                text = text_stalemate
                                                end = True
                                            elif draw(positions, d_board, cases_occupied):
                                                screen = "end"
                                                text = text_draw
                                        if turn==1:
                                            turn = 2
                                        else:
                                            turn = 1
                                    else:
                                        new_pos_x = cases_occupied[clicked] + "_x"
                                        new_pos_y = cases_occupied[clicked] + "_y"
                                        exec("%s = %d" % (new_pos_x, cases[clicked][0][0]))
                                        exec("%s = %d" % (new_pos_y, cases[clicked][0][1]))
                            clicked = None

                if event.type == pygame.MOUSEMOTION:
                    if clicked != None:
                        new_pos_x = cases_occupied[clicked] + "_x"
                        new_pos_y = cases_occupied[clicked] + "_y"
                        exec("%s = %d" % (new_pos_x, event.pos[0]))
                        exec("%s = %d" % (new_pos_y, event.pos[1]))

            if event.type == pygame.QUIT:
                running = False
                
        elif sides[turn] == "CPU":
            for event in pygame.event.get():
                print(turn)
                move=chess_ai(last_move, cases_occupied, check, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved, positions, 500,turn)
                clicked = move[0]
                case_finale = move[1]
                ligne = int(clicked[1])-1
                ligne_f = int(case_finale[1])-1
                for j in range(8):
                    if clicked == d_board[ligne][j][0]:
                        colonne = j
                for j in range(8):
                    if case_finale == d_board[ligne_f][j][0]:
                        colonne_f = j
                for i in range(8):
                    if d_board[ligne][i][0]==clicked:
                        colonne=i
                        if d_board[ligne][i][3]==turn and coup_legal(clicked, case_finale, last_move, cases_occupied, check, d_board):
                            if case_finale in cases_occupied:
                                if d_board[ligne_f][colonne_f][3] == 0:
                                    final_y = 150
                                else:
                                    final_y = 450
                                new_pos_x = cases_occupied[case_finale] + "_x"
                                new_pos_y = cases_occupied[case_finale] + "_y"
                                exec("%s = %d" % (new_pos_x, 700))
                                exec("%s = %d" % (new_pos_y, final_y))
                                del cases_occupied[case_finale]
                            elif d_board[ligne][colonne][1]=="pion":
                                if ligne_f == ligne+1 and colonne_f == colonne+1:
                                    en_passant = d_board[ligne][colonne_f][0]
                                    if d_board[ligne][colonne_f][3] == 0:
                                        final_y = 150
                                    else:
                                        final_y = 450
                                    new_pos_x = cases_occupied[en_passant] + "_x"
                                    new_pos_y = cases_occupied[en_passant] + "_y"
                                    exec("%s = %d" % (new_pos_x, 700))
                                    exec("%s = %d" % (new_pos_y, final_y))
                                    del cases_occupied[en_passant]
                                    d_board[ligne][colonne_f][1]=None
                                    d_board[ligne][colonne_f][2]=None
                                    d_board[ligne][colonne_f][3]=None
                            elif d_board[ligne][colonne][1]=="roi" and d_board[ligne][colonne][0]=="e1" and d_board[ligne_f][colonne_f][0]=="g1":
                                tour2_blanc_x = cases["f1"][0][0] 
                                tour2_blanc_y = cases["f1"][0][1]
                                d_board[0][7][1] = None
                                d_board[0][7][2] = None
                                d_board[0][7][3] = None
                                d_board[0][5][1] = "tour"
                                d_board[0][5][2] = "tour2_blanc"
                                d_board[0][5][3] = 1
                                del cases_occupied["h1"]
                                cases_occupied["f1"]="tour2_blanc"
                            elif d_board[ligne][colonne][1]=="roi" and d_board[ligne][colonne][0]=="e1" and d_board[ligne_f][colonne_f][0]=="c1":
                                tour1_blanc_x = cases["d1"][0][0]
                                tour1_blanc_y = cases["d1"][0][1]
                                d_board[0][0][1] = None
                                d_board[0][0][2] = None
                                d_board[0][0][3] = None
                                d_board[0][3][1] = "tour"
                                d_board[0][3][2] = "tour1_blanc"
                                d_board[0][3][3] = 1
                                del cases_occupied["a1"]
                                cases_occupied["d1"]="tour1_blanc"
                            elif d_board[ligne][colonne][1]=="roi" and d_board[ligne][colonne][0]=="e8" and d_board[ligne_f][colonne_f][0]=="g8":
                                tour2_noir_x = cases["f8"][0][0]
                                tour2_noir_y = cases["f8"][0][1]
                                d_board[7][7][1] = None
                                d_board[7][7][2] = None
                                d_board[7][7][3] = None
                                d_board[7][5][1] = "tour"
                                d_board[7][5][2] = "tour2_noir"
                                d_board[7][5][3] = 2
                                del cases_occupied["h8"]
                                cases_occupied["f8"]="tour2_noir"
                            elif d_board[ligne][colonne][1]=="roi" and d_board[ligne][colonne][0]=="e8" and d_board[ligne_f][colonne_f][0]=="c8":
                                tour1_noir_x = cases["d8"][0][0]
                                tour1_noir_y = cases["d8"][0][1]
                                d_board[7][0][1] = None
                                d_board[7][0][2] = None
                                d_board[7][0][3] = None
                                d_board[7][3][1] = "tour"
                                d_board[7][3][2] = "tour1_noir"
                                d_board[7][3][3] = 2
                                del cases_occupied["a8"]
                                cases_occupied["d8"]="tour1_noir"
                            if d_board[ligne][colonne][1]=="pion" and (ligne_f==7 or ligne_f==0):
                                promotion = move[2]
                                new_transfo = d_board[ligne][colonne][2] + "_transfo"
                                print(new_transfo)
                                exec(f"{new_transfo} = '{promotion}'")
                                print(pion8_blanc_transfo)
                                d_board[ligne][colonne][1]=promotion
                            new_pos_x = cases_occupied[clicked] + "_x"
                            new_pos_y = cases_occupied[clicked] + "_y"
                            exec("%s = %d" % (new_pos_x, cases[case_finale][0][0]))
                            exec("%s = %d" % (new_pos_y, cases[case_finale][0][1]))
                            if d_board[ligne][colonne][2]=="roi_blanc":
                                king_moved[0] = True
                            elif d_board[ligne][colonne][2]=="roi_noir":
                                king_moved[1] = True
                            elif d_board[ligne][colonne][2]=="tour1_blanc":
                                queen_side_rook_moved[0] = True
                            elif d_board[ligne][colonne][2]=="tour1_noir":
                                queen_side_rook_moved[1] = True
                            elif d_board[ligne][colonne][2]=="tour2_blanc":
                                king_side_rook_moved[0] = True
                            elif d_board[ligne][colonne][2]=="tour2_noir":
                                king_side_rook_moved[1] = True
                            cases_occupied[case_finale]=cases_occupied[clicked]
                            del cases_occupied[clicked]
                            last_move = [clicked, case_finale, d_board[ligne][colonne][1]]
                            d_board[ligne_f][colonne_f][1]=d_board[ligne][i][1]
                            d_board[ligne_f][colonne_f][2]=d_board[ligne][i][2]
                            d_board[ligne_f][colonne_f][3]=d_board[ligne][i][3]
                            d_board[ligne][i][1] = None
                            d_board[ligne][i][2] = None
                            d_board[ligne][i][3] = None
                            positions.append(d_board[:])
                            check = 0
                            if turn ==1:
                                for case in cases_occupied:
                                    if cases_occupied[case]=="roi_noir":
                                        case_roi = case
                            if turn ==2:
                                for case in cases_occupied:
                                    if cases_occupied[case]=="roi_blanc":
                                        case_roi = case
                            for case in cases_occupied:
                                if case_roi in coup_possiblement_legal(case, last_move, cases_occupied, check, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                    check=1
                            if check == 1:
                                if checkmate(case_roi, cases_occupied, last_move):
                                    if turn==1:
                                        text = text_white_win
                                        screen = "end"
                                    else:
                                        text = text_black_win
                                        screen = "end"
                            else:
                                if stalemate(cases_occupied, turn):
                                    text = text_stalemate
                                    screen = "end"
                                elif draw(positions, d_board, cases_occupied):
                                    screen = "end"
                                    text = text_draw
                            if turn==1:
                                turn = 2
                            else:
                                turn = 1
                        else:
                            new_pos_x = cases_occupied[clicked] + "_x"
                            new_pos_y = cases_occupied[clicked] + "_y"
                            exec("%s = %d" % (new_pos_x, cases[clicked][0][0]))
                            exec("%s = %d" % (new_pos_y, cases[clicked][0][1]))
                clicked = None
                
                if event.type == pygame.QUIT:
                    running = False
                    
    elif screen == "end":
        for event in pygame.event.get():
            end_board_setup()
            input_rect = pygame.Rect(100, 200, 400, 200)
            pygame.draw.rect(board, color, input_rect) 
            text_surface = base_font.render(text, True, (255, 255, 255)) 
            board.blit(text_surface, (input_rect.x+5, input_rect.y+5)) 
            input_rect.w = max(100, text_surface.get_width()+10)
            
            if event.type == pygame.QUIT:
                    running = False
        
    pygame.display.update()
