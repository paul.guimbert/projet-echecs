import pygame
from chess_ai_basic import chess_ai
import copy

pygame.init()

class Board:
    
    def __init__(self):
        self.LastMove = {"CaseInitiale": None, "CaseFinale": None, "ExInfoCaseFinale": None} #CaseInitiale, CaseFinale, Ancienne info de la case finale
        self.dic = {}
        #Je donne les coordonnées x et y ou faut mettre la pièce puis les x et y limites de la case
        #Puis je met le type de pièce, le nom de la pièce et finalement si elle appartient au joueur 1 ou 2 dans une liste dans la liste
        #Si il n'y a pas de pièce il y a des None
        #Pour les limites: x-gauche, x-droite, y-haut, y-bas
        for i in range(8):
            num=i+1
            self.dic[f"a{num}"] = [(5, 5+(7-i)*75), [0, 75, (8-i)*75, (7-i)*75]]
            if num>2 and num<7:
                self.dic[f"a{num}"].append(None)
            if num == 1:
                tour1_blanc = Tour("a1", 5, 5+(7-i)*75, 1)
                self.dic[f"a{num}"].append(tour1_blanc)
            elif num == 8:
                tour1_noir = Tour("a8", 5, 5+(7-i)*75, 2)
                self.dic[f"a{num}"].append(tour1_noir)
            elif num == 2:
                pion1_blanc = Pion("a2", 5, 5+(7-i)*75, 1)
                self.dic[f"a{num}"].append(pion1_blanc)
            elif num == 7:
                pion1_noir = Pion("a7", 5, 5+(7-i)*75, 2)
                self.dic[f"a{num}"].append(pion1_noir)
            
        for i in range(8):
            num=i+1
            self.dic[f"b{num}"] = [(80, 5+(7-i)*75), [75, 150, (8-i)*75, (7-i)*75]]
            if num>2 and num<7:
                self.dic[f"b{num}"].append(None)
            if num == 1:
                cavalier1_blanc = Cavalier("b1", 80, 5+(7-i)*75, 1)
                self.dic[f"b{num}"].append(cavalier1_blanc)
            elif num == 8:
                cavalier1_noir = Cavalier("b8", 80, 5+(7-i)*75, 1)
                self.dic[f"b{num}"].append(cavalier1_noir)
            elif num == 2:
                pion2_blanc = Pion("b2", 80, 5+(7-i)*75, 1)
                self.dic[f"b{num}"].append(pion2_blanc)
            elif num == 7:
                pion2_noir = Pion("b7", 80, 5+(7-i)*75, 2)
                self.dic[f"b{num}"].append(pion2_noir)


        for i in range(8):
            num=i+1
            self.dic[f"c{num}"] = [(155, 5+(7-i)*75), [150, 225, (8-i)*75, (7-i)*75]]
            if num>2 and num<7:
                self.dic[f"c{num}"].append(None)
            if num == 1:
                fou1_blanc = Fou("c1", 155, 5+(7-i)*75, 1)
                self.dic[f"c{num}"].append(fou1_blanc)
            elif num == 8:
                fou1_noir = Fou("c8", 155, 5+(7-i)*75, 2)
                self.dic[f"c{num}"].append(fou1_noir)
            elif num == 2:
                pion3_blanc = Pion("c2", 155, 5+(7-i)*75, 1)
                self.dic[f"c{num}"].append(pion3_blanc)
            elif num == 7:
                pion3_noir = Pion("c7", 155, 5+(7-i)*75, 2)
                self.dic[f"c{num}"].append(pion3_noir)

        for i in range(8):
            num=i+1
            self.dic[f"d{num}"] = [(230, 5+(7-i)*75), [225, 300, (8-i)*75, (7-i)*75]]
            if num>2 and num<7:
                self.dic[f"d{num}"].append(None)
            if num == 1:
                reine_blanc = Reine("d1", 230, 5+(7-i)*75, 1)
                self.dic[f"d{num}"].append(reine_blanc)
            elif num == 8:
                reine_noir = Reine("d8", 230, 5+(7-i)*75, 2)
                self.dic[f"d{num}"].append(reine_noir)
            elif num == 2:
                pion4_blanc = Pion("d2", 230, 5+(7-i)*75, 1)
                self.dic[f"d{num}"].append(pion4_blanc)
            elif num == 7:
                pion4_noir = Pion("d7", 230, 5+(7-i)*75, 2)
                self.dic[f"d{num}"].append(pion4_noir)

        for i in range(8):
            num=i+1
            self.dic[f"e{num}"] = [(305, 5+(7-i)*75), [300, 375, (8-i)*75, (7-i)*75]]
            if num>2 and num<7:
                self.dic[f"e{num}"].append(None)
            if num == 1:
                roi_blanc = Roi("e1", 305, 5+(7-i)*75, 1)
                self.dic[f"e{num}"].append(roi_blanc)
            elif num == 8:
                roi_noir = Roi("e8", 305, 5+(7-i)*75, 2)
                self.dic[f"e{num}"].append(roi_noir)
            elif num == 2:
                pion5_blanc = Pion("e2", 305, 5+(7-i)*75, 1)
                self.dic[f"e{num}"].append(pion5_blanc)
            elif num == 7:
                pion5_noir = Pion("e7", 305, 5+(7-i)*75, 2)
                self.dic[f"e{num}"].append(pion5_noir)

        for i in range(8):
            num=i+1
            self.dic[f"f{num}"] = [(380, 5+(7-i)*75), [375, 450, (8-i)*75, (7-i)*75]]
            if num>2 and num<7:
                self.dic[f"f{num}"].append(None)
            if num == 1:
                fou2_blanc = Fou("f1", 380, 5+(7-i)*75, 1)
                self.dic[f"f{num}"].append(fou2_blanc)
            elif num == 8:
                fou2_noir = Fou("f8", 380, 5+(7-i)*75, 2)
                self.dic[f"f{num}"].append(fou2_noir)
            elif num == 2:
                pion6_blanc = Pion("f2", 380, 5+(7-i)*75, 1)
                self.dic[f"f{num}"].append(pion6_blanc)
            elif num == 7:
                pion6_noir = Pion("f7", 380, 5+(7-i)*75, 2)
                self.dic[f"f{num}"].append(pion6_noir)

        for i in range(8):
            num=i+1
            self.dic[f"g{num}"] = [(455, 5+(7-i)*75), [450, 525, (8-i)*75, (7-i)*75]]
            if num>2 and num<7:
                self.dic[f"g{num}"].append(None)
            if num == 1:
                cavalier2_blanc = Cavalier("g1", 455, 5+(7-i)*75, 1)
                self.dic[f"g{num}"].append(cavalier2_blanc)
            elif num == 8:
                cavalier2_noir = Cavalier("g8", 455, 5+(7-i)*75, 2)
                self.dic[f"g{num}"].append(cavalier2_noir)
            elif num == 2:
                pion7_blanc = Pion("g2", 455, 5+(7-i)*75, 1)
                self.dic[f"g{num}"].append(pion7_blanc)
            elif num == 7:
                pion7_noir = Pion("g7", 455, 5+(7-i)*75, 2)
                self.dic[f"g{num}"].append(pion7_noir)

        for i in range(8):
            num=i+1
            self.dic[f"h{num}"] = [(530, 5+(7-i)*75), [525, 600, (8-i)*75, (7-i)*75]]
            if num>2 and num<7:
                self.dic[f"h{num}"].append(None)
            if num == 1:
                tour2_blanc = Tour("h1", 530, 5+(7-i)*75, 1)
                self.dic[f"h{num}"].append(tour2_blanc)
            elif num == 8:
                tour2_noir = Tour("h8", 530, 5+(7-i)*75, 2)
                self.dic[f"h{num}"].append(tour2_noir)
            elif num == 2:
                pion8_blanc = Pion("h2", 530, 5+(7-i)*75, 1)
                self.dic[f"h{num}"].append(pion8_blanc)
            elif num == 7:
                pion8_noir = Pion("h7", 530, 5+(7-i)*75, 2)
                self.dic[f"h{num}"].append(pion8_noir)
                
    def Move(self, CaseInitiale, CaseFinale):
            self.LastMove = {"CaseInitiale": CaseInitiale, "CaseFinale": CaseFinale, "ExInfoCaseFinale": self.dic[CaseFinale][2]}
            self.dic[CaseFinale][2] = self.dic[CaseInitiale][2]
            if type(self.dic[CaseFinale][2]) is Roi or type(self.dic[CaseFinale][2]) is Tour:
                self.dic[CaseFinale][2].Moved = True
            self.dic[CaseInitiale][2] = None
        
    def RevertMove(self):
        self.dic[self.LastMove["CaseInitiale"]][2] = self.dic[self.LastMove["CaseFinale"]][2]
        self.dic[self.LastMove["CaseFinale"]][2] = self.LastMove["ExInfoCaseFinale"]
    
    def AllLegalMovesWithoutPins(self):
        pass
        #Use the possible moves from the pieces subclasses
        #Disjonction de cas pour si il y a promotion
        
    def PinCheck():
        pass
        #Check if the possibly legal move is good or not (whether there was a pin or not)
        
    def CheckGameOver():
        pass

class GameInterface:
    turn = 1
    
    def __init__(self):
        self.board = pygame.display.set_mode((800,600))
    
    #Création de l'échiquier
    def board_setup(self):
        self.board.fill((229, 231, 233))
        for x in range(8):
            for y in range(8):
                if (x+y)%2==1:
                    pygame.draw.rect(self.board, (81, 90, 90), (x*75, y*75, 75, 75))
        pygame.draw.rect(self.board, (255,255,255), (600, 0, 200, 600))
        
        
    def end_board_setup(self):
        self.board.fill((((229, 231, 233))))
        
    def human_movement():
        pass
        #Does the check for movement and the different cases for mouseup, mouse movement ....
        #Makes the piece move with you but doesn't do anything else
        
    def move():
        pass
        #2 cases: AI move or human move, if AI move no checks
        #Checks if the move is legal and does all the things in the old while loop
        #Then if it is legal places the piece on the new square or else on the old square
         
        
    
class Pieces:
    
    def __init__(self, square, x_initial, y_initial, side):
        self.position = square
        self.x = x_initial
        self.y = y_initial
        self.side = side
        
    def MovePiece(self, square, new_x, new_y):
        self.position = square
        #Get x and y ccordinates for square and update the piece x and y coordinates
        
    def LineColumn(square, dboard):
        ligne = int(square[1])-1
        for i in range(8):
            if square== dboard[ligne][i][0]:
                colonne = i
        return(ligne, colonne)
    
    def GetSquareName(ligne, colonne):
        return(chr(97+colonne-1)+str(ligne))
    
    #Centralisation de la vérification de si un coup est bien sur une case vide ou la prise d'un adversaire et pas la prise d'une de ses propres pièces
    def LegalMoves(self, PossibleSquares, dboard):
        coups_possibles = []
        for PossibleSquare in PossibleSquares:
            if dboard[PossibleSquare][2] == None:
                coups_possibles.append(dboard[PossibleSquare])
            elif dboard[PossibleSquare][2].side != self.side:
                coups_possibles.append(dboard[PossibleSquare])
        return coups_possibles
        
        
class Pion(Pieces):
    
    def __init__(self, square, x_initial, y_initial, side, transformation = None):
        super().__init__(square, x_initial, y_initial, side)
        self.transformation = transformation
        
    def LegalMoves(self, dboard, last_move):
        (ligne, colonne) = super().LineColumn(self.position)
        PossibleSquares = []
        coups_possibles = []
        if self.side == 1:
            if ligne == 1:
                if dboard[super().GetSquareName(ligne+1, colonne)][2] == None and dboard[super().GetSquareName(ligne+2, colonne)][2] == None:
                    coups_possibles.append(super().GetSquareName(ligne+2, colonne))
            elif ligne == 4 and (type(last_move["ExInfoCaseFinale"]) is Pion):
                if colonne + 1 <8:
                    if last_move["CaseInitiale"]==super().GetSquareName(ligne+2, colonne+1) and last_move["CaseFinale"]==super().GetSquareName(ligne, colonne+1):
                        coups_possibles.append(super().GetSquareName(ligne+1, colonne+1))
                if colonne - 1 >=0:
                    if last_move["CaseInitiale"]==super().GetSquareName(ligne+2, colonne-1) and last_move["CaseFinale"]==super().GetSquareName(ligne, colonne-1):
                        coups_possibles.append(super().GetSquareName(ligne+1, colonne-1))
            if dboard[super().GetSquareName(ligne+1, colonne)][2] == None:
                PossibleSquares.append(super().GetSquareName(ligne+1, colonne))
            if colonne+1<8:
                if dboard[super().GetSquareName(ligne+1, colonne+1)][2] != None and dboard[super().GetSquareName(ligne+1, colonne+1)][2].side == 2:
                    PossibleSquares.append(super().GetSquareName(ligne+1, colonne+1))
            if colonne-1>=0:
                if dboard[super().GetSquareName(ligne+1, colonne-1)][2] != None and dboard[super().GetSquareName(ligne+1, colonne-1)][2].side == 2:
                    PossibleSquares.append(super().GetSquareName(ligne+1, colonne-1))
            
            #Coup normal, pas de promotion, le pion restera un pion
            if ligne < 6:
                for PossibleSquare in PossibleSquares:
                    coups_possibles.append(PossibleSquare)
                    
            #Promotion, on ajoute les différents cas à l'aide d'un tuple
            if ligne == 6:
                for PossibleSquare in PossibleSquares:
                    coups_possibles.append((PossibleSquare, "fou"))
                    coups_possibles.append((PossibleSquare, "cavalier"))
                    coups_possibles.append((PossibleSquare, "tour"))
                    coups_possibles.append((PossibleSquare, "reine"))
            
        elif self.side == 2:
            if ligne == 6:
                if dboard[super().GetSquareName(ligne-1, colonne)][2] == None and dboard[super().GetSquareName(ligne-2, colonne)][2] == None:
                    coups_possibles.append(super().GetSquareName(ligne-2, colonne))
            elif ligne == 3 and (type(last_move["ExInfoCaseFinale"]) is Pion):
                if colonne + 1 <8:
                    if last_move["CaseInitiale"]==super().GetSquareName(ligne-2, colonne+1) and last_move["CaseFinale"]==super().GetSquareName(ligne, colonne+1):
                        coups_possibles.append(super().GetSquareName(ligne-1, colonne+1))
                if colonne - 1 >=0:
                    if last_move["CaseInitiale"]==super().GetSquareName(ligne-2, colonne-1) and last_move["CaseFinale"]==super().GetSquareName(ligne, colonne-1):
                        coups_possibles.append(super().GetSquareName(ligne-1, colonne-1))
            if dboard[super().GetSquareName(ligne-1, colonne)][2] == None:
                PossibleSquares.append(super().GetSquareName(ligne-1, colonne))
            if colonne+1<8:
                if dboard[super().GetSquareName(ligne-1, colonne+1)][2] != None and dboard[super().GetSquareName(ligne-1, colonne+1)][2].side == 2:
                    PossibleSquares.append(super().GetSquareName(ligne-1, colonne+1))
            if colonne-1>=0:
                if dboard[super().GetSquareName(ligne-1, colonne-1)][2] != None and dboard[super().GetSquareName(ligne-1, colonne-1)][2].side == 2:
                    PossibleSquares.append(super().GetSquareName(ligne-1, colonne-1))
                    
            #Coup normal, pas de promotion, le pion restera un pion
            if ligne > 1:
                for PossibleSquare in PossibleSquares:
                    coups_possibles.append(PossibleSquare)
                    
            #Promotion, on ajoute les différents cas à l'aide d'un tuple
            if ligne == 1:
                for PossibleSquare in PossibleSquares:
                    coups_possibles.append((PossibleSquare, "fou"))
                    coups_possibles.append((PossibleSquare, "cavalier"))
                    coups_possibles.append((PossibleSquare, "tour"))
                    coups_possibles.append((PossibleSquare, "reine"))
                    
        return coups_possibles

class Tour(Pieces):
    
    def __init__(self, square, x_initial, y_initial, side, moved=False):
        super().__init__(square, x_initial, y_initial, side)
        self.moved = moved
        
    def LegalMoves(self, dboard):
        (ligne, colonne) = super().LineColumn(self.position)
        i=colonne + 1
        occupation = None
        PossibleSquares = []
        while i<8 and occupation == None:
            PossibleSquare = super().GetSquareName(ligne, i)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            i+=1
        occupation = None
        i = colonne -1
        while i>=0 and occupation == None:
            PossibleSquare = super().GetSquareName(ligne, i)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            i-=1
        occupation = None
        j = ligne + 1
        while j<8 and occupation == None:
            PossibleSquare = super().GetSquareName(j, colonne)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            j+=1
        occupation = None
        j = ligne -1
        while j>=0 and occupation == None:
            PossibleSquare = super().GetSquareName(j, colonne)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            j-=1
        return(super().LegalMoves(PossibleSquares, dboard))

class Cavalier(Pieces):
    
    def __init__(self, square, x_initial, y_initial, side):
        super().__init__(square, x_initial, y_initial, side)
        
    def LegalMoves(self, dboard):
        (ligne, colonne) = super().LineColumn(self.position)
        col_a = colonne + 2
        col_b = colonne + 1
        col_c = colonne - 1
        col_d = colonne - 2
        lig_a = ligne + 2
        lig_b = ligne + 1
        lig_c = ligne - 1
        lig_d = ligne - 2
        PossibleSquares = []
        if col_a < 8:
            if lig_b < 8:
                PossibleSquares.append(super().GetSquareName(lig_b, col_a))
            if lig_c >= 0:
                PossibleSquares.append(super().GetSquareName(lig_c, col_a))
        if col_d >= 0:
            if lig_b < 8:
                PossibleSquares.append(super().GetSquareName(lig_b, col_d))
            if lig_c >= 0:
                PossibleSquares.append(super().GetSquareName(lig_c, col_d))
        if col_b < 8:
            if lig_a < 8:
                PossibleSquares.append(super().GetSquareName(lig_a, col_b))
            if lig_d >= 0:
                PossibleSquares.append(super().GetSquareName(lig_d, col_b)) 
        if col_c >= 0:
            if lig_a < 8:
                PossibleSquares.append(super().GetSquareName(lig_a, col_c))
            if lig_d >= 0:
                PossibleSquares.append(super().GetSquareName(lig_d, col_c))
        return(super().LegalMoves(PossibleSquares, dboard))

class Fou(Pieces):
    
    def __init__(self, square, x_initial, y_initial, side):
        super().__init__(square, x_initial, y_initial, side)
        
    def LegalMoves(self, dboard):
        (ligne, colonne) = super().LineColumn(self.position)
        PossibleSquares = []
        a=ligne+1
        b=colonne+1
        occupation = None
        while a<8 and b<8 and occupation==None:
            PossibleSquare = super().GetSquareName(a, b)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            a+=1
            b+=1
        a=ligne+1
        b=colonne-1
        occupation = None
        while a<8 and b>=0 and occupation == None:
            PossibleSquare = super().GetSquareName(a, b)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            a+=1
            b-=1
        a=ligne-1
        b=colonne-1
        occupation = None
        while a>=0 and b>=0 and occupation == None:
            PossibleSquare = super().GetSquareName(a, b)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            a-=1
            b-=1
        a=ligne-1
        b=colonne+1
        occupation = None
        while a>=0 and b<8 and occupation == None:
            PossibleSquare = super().GetSquareName(a, b)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            a-=1
            b+=1
        return(super().LegalMoves(PossibleSquares, dboard))

class Reine(Pieces):
    
    def __init__(self, square, x_initial, y_initial, side):
        super().__init__(square, x_initial, y_initial, side)
        
    def LegalMoves(self, dboard):
        (ligne, colonne) = super().LineColumn(self.position)
        PossibleSquares = []
        a=ligne+1
        b=colonne+1
        occupation = None
        while a<8 and b<8 and occupation==None:
            PossibleSquare = super().GetSquareName(a, b)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            a+=1
            b+=1
        a=ligne+1
        b=colonne-1
        occupation = None
        while a<8 and b>=0 and occupation == None:
            PossibleSquare = super().GetSquareName(a, b)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            a+=1
            b-=1
        a=ligne-1
        b=colonne-1
        occupation = None
        while a>=0 and b>=0 and occupation == None:
            PossibleSquare = super().GetSquareName(a, b)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            a-=1
            b-=1
        a=ligne-1
        b=colonne+1
        occupation = None
        while a>=0 and b<8 and occupation == None:
            PossibleSquare = super().GetSquareName(a, b)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            a-=1
            b+=1
        i=colonne + 1
        occupation = None
        while i<8 and occupation == None:
            PossibleSquare = super().GetSquareName(ligne, i)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            i+=1
        occupation = None
        i = colonne -1
        while i>=0 and occupation == None:
            PossibleSquare = super().GetSquareName(ligne, i)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            i-=1
        occupation = None
        j = ligne + 1
        while j<8 and occupation == None:
            PossibleSquare = super().GetSquareName(j, colonne)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            j+=1
        occupation = None
        j = ligne -1
        while j>=0 and occupation == None:
            PossibleSquare = super().GetSquareName(j, colonne)
            PossibleSquares.append(PossibleSquare)
            occupation = dboard[PossibleSquare][2]
            j-=1
        return(super().LegalMoves(PossibleSquares, dboard))
        
class Roi(Pieces):
    
    moved = False
    
    def __init__(self, square, x_initial, y_initial, side):
        super().__init__(square, x_initial, y_initial, side)
        
    def LegalMoves(self, dboard):
        coups_possibles = []
        
        #Les coups ou on ne roc pas
        (ligne, colonne) = super().LineColumn(self.position)
        for i in range(-1,2):
            for j in range(-1,2):
                if i!=0 or j!=0:
                    PossibleSquare = super().GetSquareName(i,j)
                    if ligne+i>=0 and ligne+i<8 and colonne+j>=0 and colonne+j<8 and dboard[PossibleSquare][2]==None:
                        coups_possibles.append(PossibleSquare)
        
        #Les possibles roc
        if self.position == "e1" and self.side == 1 and self.moved == False:
            if type(dboard["a1"][2]) is Tour and dboard["a1"][2].moved == False and dboard["b1"][2] == None and dboard["c1"][2] == None and dboard["d1"][2] == None:
                coups_possibles.append(("b1", "Roc"))
            elif type(dboard["h1"][2]) is Tour and dboard["h1"][2].moved == False and dboard["g1"][2] == None and dboard["f1"][2] == None:
                coups_possibles.append(("g1", "Roc"))
        elif self.position == "e8" and self.side == 2 and self.moved == False:
            if type(dboard["a8"][2]) is Tour and dboard["a8"][2].moved == False and dboard["b8"][2] == None and dboard["c8"][2] == None and dboard["d8"][2] == None:
                coups_possibles.append(("b8", "Roc"))
            elif type(dboard["h8"][2]) is Tour and dboard["h8"][2].moved == False and dboard["g8"][2] == None and dboard["f8"][2] == None:
                coups_possibles.append(("g8", "Roc"))
        return coups_possibles



        
#Game loop invoking things from the GameIntefrace Class