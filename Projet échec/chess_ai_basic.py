import random
from anytree import Node, RenderTree
import numpy as np
import copy

#Programme un peu plus complexe qui utilise l'algo de Monte-Carlo pour explorer un peu l'arbre de possibilités. 
#Cependant, la fonction d'évaluation est très simple: je calcul juste le nombre de points par coté en additionnant la valeure
#des pièces restantes (Pion:1, Cavalier: 3, Fou:3, Tour: 5, Reine:9)
#Prochaine étape: créer une fonction d'évaluation plus aboutie

def coup_possiblement_legal(case_actuelle, last_move, cases_occupied_l, checked, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
    ligne = int(case_actuelle[1])-1
    coups_possibles = []
    for i in range(8):
        if case_actuelle == d_board_bis[ligne][i][0]:
            colonne = i
    if d_board_bis[ligne][colonne][1] == "tour" or d_board_bis[ligne][colonne][1] == "reine":
        i=colonne + 1
        while i<8:
            if d_board_bis[ligne][i][1] == None:
                coups_possibles.append(d_board_bis[ligne][i][0])
                i+=1
            else:
                if d_board_bis[ligne][i][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[ligne][i][0])
                i=8
        i = colonne -1
        while i>=0:
            if d_board_bis[ligne][i][1] == None:
                coups_possibles.append(d_board_bis[ligne][i][0])
                i-=1
            else:
                if d_board_bis[ligne][i][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[ligne][i][0])
                i=-1
        j = ligne + 1
        while j<8:
            if d_board_bis[j][colonne][1] == None:
                coups_possibles.append(d_board_bis[j][colonne][0])
                j+=1
            else:
                if d_board_bis[j][colonne][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[j][colonne][0])
                j=8
        j = ligne -1
        while j>=0:
            if d_board_bis[j][colonne][1] == None:
                coups_possibles.append(d_board_bis[j][colonne][0])
                j-=1
            else:
                if d_board_bis[j][colonne][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[j][colonne][0])
                j=-1
    elif d_board_bis[ligne][colonne][1] == "cavalier":
        col_a = colonne + 2
        col_b = colonne + 1
        col_c = colonne - 1
        col_d = colonne - 2
        lig_a = ligne + 2
        lig_b = ligne + 1
        lig_c = ligne - 1
        lig_d = ligne - 2
        if col_a < 8:
            if lig_b < 8:
                if d_board_bis[lig_b][col_a][1] == None or d_board_bis[lig_b][col_a][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_b][col_a][0])
            if lig_c >= 0:
                if d_board_bis[lig_c][col_a][1] == None or d_board_bis[lig_c][col_a][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_c][col_a][0]) 
        if col_d >= 0:
            if lig_b < 8:
                if d_board_bis[lig_b][col_d][1] == None or d_board_bis[lig_b][col_d][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_b][col_d][0])
            if lig_c >= 0:
                if d_board_bis[lig_c][col_d][1] == None or d_board_bis[lig_c][col_d][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_c][col_d][0])
        if col_b < 8:
            if lig_a < 8:
                if d_board_bis[lig_a][col_b][1] == None or d_board_bis[lig_a][col_b][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_a][col_b][0])
            if lig_d >= 0:
                if d_board_bis[lig_d][col_b][1] == None or d_board_bis[lig_d][col_b][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_d][col_b][0]) 
        if col_c >= 0:
            if lig_a < 8:
                if d_board_bis[lig_a][col_c][1] == None or d_board_bis[lig_a][col_c][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_a][col_c][0])
            if lig_d >= 0:
                if d_board_bis[lig_d][col_c][1] == None or d_board_bis[lig_d][col_c][3] != d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[lig_d][col_c][0])
    if d_board_bis[ligne][colonne][1] == "fou" or d_board_bis[ligne][colonne][1] == "reine":
        a=ligne+1
        b=colonne+1
        while a<8 and b<8:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a+=1
                b+=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=8
                b=8
        a=ligne+1
        b=colonne-1
        while a<8 and b>=0:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a+=1
                b-=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=8
                b=-1
        a=ligne-1
        b=colonne-1
        while a>=0 and b>=0:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a-=1
                b-=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=-1
                b=-1
        a=ligne-1
        b=colonne+1
        while a>=0 and b<8:
            if d_board_bis[a][b][1] == None:
                coups_possibles.append(d_board_bis[a][b][0])
                a-=1
                b+=1
            else:
                if d_board_bis[a][b][3]!=d_board_bis[ligne][colonne][3]:
                    coups_possibles.append(d_board_bis[a][b][0])
                a=-1
                b=8
    elif d_board_bis[ligne][colonne][1] == "pion":
        if d_board_bis[ligne][colonne][3] == 1:
            if ligne<7:
                if ligne == 1:
                    if d_board_bis[ligne+1][colonne][1] == None and d_board_bis[ligne+2][colonne][1] == None:
                        coups_possibles.append(d_board_bis[ligne+2][colonne][0])
                elif ligne == 4:
                    if colonne + 1 <8:
                        if last_move[0]==d_board_bis[ligne+2][colonne+1][0] and last_move[1]==d_board_bis[ligne][colonne+1][0] and last_move[2]=="pion":
                            coups_possibles.append(d_board_bis[ligne+1][colonne+1][0])
                    if colonne - 1 >=0:
                        if last_move[0]==d_board_bis[ligne+2][colonne-1][0] and last_move[1]==d_board_bis[ligne][colonne-1][0] and last_move[2]=="pion":
                            coups_possibles.append(d_board_bis[ligne+1][colonne-1][0])
                if d_board_bis[ligne+1][colonne][1] == None:
                    coups_possibles.append(d_board_bis[ligne+1][colonne][0])
                if colonne+1<8:
                    if d_board_bis[ligne+1][colonne+1][3] == 2:
                        coups_possibles.append(d_board_bis[ligne+1][colonne+1][0])
                if colonne-1>=0:
                    if d_board_bis[ligne+1][colonne-1][3] == 2:
                        coups_possibles.append(d_board_bis[ligne+1][colonne-1][0])
        elif d_board_bis[ligne][colonne][3] == 2:
            if ligne>0:
                if ligne == 6:
                    if d_board_bis[ligne-1][colonne][1] == None and d_board_bis[ligne-2][colonne][1]== None:
                        coups_possibles.append(d_board_bis[ligne-2][colonne][0])
                elif ligne == 3:
                    if colonne + 1 <8:
                        if last_move[0]==d_board_bis[ligne-2][colonne+1][0] and last_move[1]==d_board_bis[ligne][colonne+1][0] and last_move[2]=="pion":
                            coups_possibles.append(d_board_bis[ligne-1][colonne+1][0])
                    if colonne - 1 >=0:
                        if last_move[0]==d_board_bis[ligne-2][colonne-1][0] and last_move[1]==d_board_bis[ligne][colonne-1][0] and last_move[2]=="pion":
                            coups_possibles.append(d_board_bis[ligne-1][colonne-1][0])
                if d_board_bis[ligne-1][colonne][1] == None:
                    coups_possibles.append(d_board_bis[ligne-1][colonne][0])
                if colonne+1<8:
                    if d_board_bis[ligne-1][colonne+1][3] == 1:
                        coups_possibles.append(d_board_bis[ligne-1][colonne+1][0])
                if colonne-1>=0:
                    if d_board_bis[ligne-1][colonne-1][3] == 1:
                        coups_possibles.append(d_board_bis[ligne-1][colonne-1][0])
    if d_board_bis[ligne][colonne][1] == "roi":
        for i in range(-1,2):
            for j in range(-1,2):
                if i!=0 or j!=0:
                    if ligne+i>=0 and ligne+i<8 and colonne+j>=0 and colonne+j<8:
                        if d_board_bis[ligne+i][colonne+j][3]==None or d_board_bis[ligne+i][colonne+j][3]!=d_board_bis[ligne][colonne][3]:
                            case_possible = d_board_bis[ligne+i][colonne+j][0]
                            if last_move != None:
                                if last_move[2]!="roi":
                                    prise_roi = 0
                                    for case in cases_occupied_l:
                                        ligne_o = int(case[1])-1
                                        for p in range(8):
                                            if case == d_board_bis[ligne_o][p][0]:
                                                colonne_o = p
                                        if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                                            cases_occupied_bis = cases_occupied_l.copy()
                                            cases_occupied_bis[case_possible]=cases_occupied_l[case_actuelle]
                                            del cases_occupied_bis[case_actuelle]
                                            ligne_possible = ligne+i
                                            colonne_possible = colonne+j
                                            prem = d_board_bis[ligne_possible][colonne_possible][1]
                                            deux = d_board_bis[ligne_possible][colonne_possible][2]
                                            trois = d_board_bis[ligne_possible][colonne_possible][3]
                                            d_board_bis[ligne_possible][colonne_possible][1]=d_board_bis[ligne][colonne][1]
                                            d_board_bis[ligne_possible][colonne_possible][2]=d_board_bis[ligne][colonne][2]
                                            d_board_bis[ligne_possible][colonne_possible][3]=d_board_bis[ligne][colonne][3]
                                            d_board_bis[ligne][colonne][1] = None
                                            d_board_bis[ligne][colonne][2] = None
                                            d_board_bis[ligne][colonne][3] = None
                                            if case_possible in coup_possiblement_legal(case, [case_actuelle, case_possible, "roi"], cases_occupied_bis, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                                prise_roi = 1
                                            d_board_bis[ligne][colonne][1]=d_board_bis[ligne_possible][colonne_possible][1]
                                            d_board_bis[ligne][colonne][2]=d_board_bis[ligne_possible][colonne_possible][2]
                                            d_board_bis[ligne][colonne][3]=d_board_bis[ligne_possible][colonne_possible][3]
                                            d_board_bis[ligne_possible][colonne_possible][1] = prem
                                            d_board_bis[ligne_possible][colonne_possible][2] = deux
                                            d_board_bis[ligne_possible][colonne_possible][3] = trois
                                    if prise_roi == 0:
                                        coups_possibles.append(d_board_bis[ligne+i][colonne+j][0])
                                else:
                                    coups_possibles.append(d_board_bis[ligne+i][colonne+j][0])
        if d_board_bis[ligne][colonne][3]==1 and not king_moved[0] and checked==0:
            if "f1" not in cases_occupied_l and "g1" not in cases_occupied_l and not king_side_rook_moved[0]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "f1" in coup_possiblement_legal(case, [case_actuelle, "g1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "g1" in coup_possiblement_legal(case, [case_actuelle, "g1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "h1" in coup_possiblement_legal(case, [case_actuelle, "g1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="g2" or d_board_bis[ligne_o][colonne_o][0]=="h2":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("g1")
            elif "b1" not in cases_occupied_l and "c1" not in cases_occupied_l and "d1" not in cases_occupied_l and not queen_side_rook_moved[0]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "a1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "b1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "c1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "d1" in coup_possiblement_legal(case, [case_actuelle, "c1", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="a2" or d_board_bis[ligne_o][colonne_o][0]=="b2" or d_board_bis[ligne_o][colonne_o][0]=="c2":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("c1")
        elif d_board_bis[ligne][colonne][3]==2 and not king_moved[1] and checked==0:
            if "f8" not in cases_occupied_l and "g8" not in cases_occupied_l and not king_side_rook_moved[1]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "f8" in coup_possiblement_legal(case, [case_actuelle, "g8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "g8" in coup_possiblement_legal(case, [case_actuelle, "g8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "h8" in coup_possiblement_legal(case, [case_actuelle, "g8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="g7" or d_board_bis[ligne_o][colonne_o][0]=="h7":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("g8")
            elif "b8" not in cases_occupied_l and "c8" not in cases_occupied_l and "d8" not in cases_occupied_l and not queen_side_rook_moved[1]:
                pas_possible=False
                for case in cases_occupied_l:
                    ligne_o = int(case[1])-1
                    for p in range(8):
                        if case == d_board_bis[ligne_o][p][0]:
                            colonne_o = p
                    if d_board_bis[ligne_o][colonne_o][3] != d_board_bis[ligne][colonne][3]:
                        if d_board_bis[ligne_o][colonne_o][1]!="roi":
                            if "a8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "b8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "c8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved) or "d8" in coup_possiblement_legal(case, [case_actuelle, "c8", "roi"], cases_occupied_l, 0, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved):
                                pas_possible=True
                        else:
                            if d_board_bis[ligne_o][colonne_o][0]=="a7" or d_board_bis[ligne_o][colonne_o][0]=="b7" or d_board_bis[ligne_o][colonne_o][0]=="c7":
                                pas_possible=True
                if not pas_possible:
                    coups_possibles.append("c8")
    return coups_possibles

def coup_legal(case_actuelle, case_finale, last_move, cases_occupied_l, checked, d_board_l, king_moved, king_side_rook_moved, queen_side_rook_moved):
    coups_possibles = coup_possiblement_legal(case_actuelle, last_move, cases_occupied_l, checked, d_board_l, king_moved, king_side_rook_moved, queen_side_rook_moved)
    ligne = int(case_actuelle[1])-1
    for i in range(8):
        if case_actuelle == d_board_l[ligne][i][0]:
            colonne = i
    if d_board_l[ligne][colonne][1] != "roi":
        case_roi = ModuleNotFoundError
        for i in range(8):
            for j in range(8):
                if d_board_l[i][j][1]=="roi":
                    if d_board_l[i][j][3]==d_board_l[ligne][colonne][3]:
                        case_roi = d_board_l[i][j][0]
        if case_roi==None:
            print(d_board_l)
        rejets = []
        for x in range(len(coups_possibles)):
            case_possible = coups_possibles[x]
            prise_roi = 0
            cases_occupied_bis = cases_occupied_l.copy()
            cases_occupied_bis[case_possible]=cases_occupied_l[case_actuelle]
            del cases_occupied_bis[case_actuelle]
            ligne_possible = int(case_possible[1])-1
            for i in range(8):
                if case_possible == d_board_l[ligne_possible][i][0]:
                    colonne_possible = i
            prem = d_board_l[ligne_possible][colonne_possible][1]
            deux = d_board_l[ligne_possible][colonne_possible][2]
            trois = d_board_l[ligne_possible][colonne_possible][3]
            d_board_l[ligne_possible][colonne_possible][1]=d_board_l[ligne][colonne][1]
            d_board_l[ligne_possible][colonne_possible][2]=d_board_l[ligne][colonne][2]
            d_board_l[ligne_possible][colonne_possible][3]=d_board_l[ligne][colonne][3]
            d_board_l[ligne][colonne][1] = None
            d_board_l[ligne][colonne][2] = None
            d_board_l[ligne][colonne][3] = None
            for case in cases_occupied_bis:
                ligne_o = int(case[1])-1
                for p in range(8):
                    if case == d_board_l[ligne_o][p][0]:
                        colonne_o = p
                if d_board_l[ligne_o][colonne_o][3] != d_board_l[ligne][colonne][3]:
                    if case_roi in coup_possiblement_legal(case, [case_actuelle, case_possible, d_board_l[ligne][colonne][3]], cases_occupied_bis, 0, d_board_l, king_moved, king_side_rook_moved, queen_side_rook_moved):
                        prise_roi = 1
            if prise_roi == 1:
                rejets.append(x)
            d_board_l[ligne][colonne][1]=d_board_l[ligne_possible][colonne_possible][1]
            d_board_l[ligne][colonne][2]=d_board_l[ligne_possible][colonne_possible][2]
            d_board_l[ligne][colonne][3]=d_board_l[ligne_possible][colonne_possible][3]
            d_board_l[ligne_possible][colonne_possible][1] = prem
            d_board_l[ligne_possible][colonne_possible][2] = deux
            d_board_l[ligne_possible][colonne_possible][3] = trois
        decal = 0
        for x in rejets:
            del coups_possibles[x-decal]
            decal += 1
    if case_finale in coups_possibles:
        return True
    else:
        return False
    
def checkmate(case_actuelle, cases_occupied_l, last_move, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
    ligne = int(case_actuelle[1])-1
    for i in range(8):
        if case_actuelle == d_board[ligne][i][0]:
            colonne = i
    for case in cases_occupied_l:
        ligne_c = int(case[1])-1
        for i in range(8):
            if case == d_board[ligne_c][i][0]:
                colonne_c = i
        if d_board[ligne_c][colonne_c][3]==d_board[ligne][colonne][3]:
            for move in coup_possiblement_legal(case, last_move, cases_occupied_l, 1, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
                if coup_legal(case, move, last_move, cases_occupied_l, 1, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
                    return False
    return True
                
def stalemate(cases_occupied_l, who_turn, d_board, last_move, king_moved, king_side_rook_moved, queen_side_rook_moved):
    if who_turn==1:
        who_turn = 2
    else:
        who_turn = 1
    for case in cases_occupied_l:
        ligne = int(case[1])-1
        for i in range(8):
            if case == d_board[ligne][i][0]:
                colonne = i
        if d_board[ligne][colonne][3]==who_turn:
            for move in coup_possiblement_legal(case, last_move, cases_occupied_l, 0, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
                if coup_legal(case, move, last_move, cases_occupied_l, 0, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
                    return False
    return True

def draw(positions, d_board_l, cases_occupied_l):
    for position in positions:
        if position[0]==d_board_l:
            position[1] += 1
            if position[1]==3:
                return True
    if len(cases_occupied_l)<=5:
        white_pieces = []
        black_pieces = []
        for case in cases_occupied_l:
            ligne_c = int(case[1])-1
            for j in range(8):
                if case == d_board_l[ligne_c][j][0]:
                    colonne_c = j
            if ligne_c == 1:
                white_pieces.append(d_board_l[ligne_c][colonne_c][1])
            else:
                black_pieces.append(d_board_l[ligne_c][colonne_c][1])
        if "pion" not in white_pieces and "tour" not in white_pieces and "reine" not in white_pieces and "pion" not in black_pieces and "tour" not in black_pieces and "reine" not in black_pieces:
            if len(white_pieces)==3:
                if "fou" not in black_pieces:
                    cavalier_count=0
                    for piece in white_pieces:
                        if piece == "cavalier":
                            cavalier_count += 1
                    if cavalier_count == 2:
                        return True
            elif len(black_pieces)==3:
                if "fou" not in white_pieces:
                    cavalier_count=0
                    for piece in black_pieces:
                        if piece == "cavalier":
                            cavalier_count += 1
                    if cavalier_count == 2:
                        return True
            elif len(cases_occupied_l)<=4:
                return True
    return False

def possible_next_positions(d_board, last_move, cases_occupied_l, checked, king_moved, king_side_rook_moved, queen_side_rook_moved, turn, positions, side):
    coups_possibles=[]
    for case in cases_occupied_l:
        ligne = int(case[1])-1
        for i in range(8):
            if case == d_board[ligne][i][0]:
                colonne = i
        if d_board[ligne][colonne][3]==turn:
            coups_potentiels = coup_possiblement_legal(case,last_move, cases_occupied_l, checked, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved)
            for j in range(len(coups_potentiels)):
                coup = coups_potentiels[j]
                if coup_legal(case, coup, last_move, cases_occupied_l, checked, d_board, king_moved, king_side_rook_moved, queen_side_rook_moved):
                    if d_board[ligne][colonne][1]=="pion" and ((ligne == 6 and (int(coup[1])-1)==7) or (ligne==1 and (int(coup[1])-1)==0)):
                            coups_possibles.append([case, coup, "reine"])
                            coups_possibles.append([case, coup, "cavalier"])
                            coups_possibles.append([case, coup, "tour"])
                            coups_possibles.append([case, coup, "fou"])
                    else:
                        coups_possibles.append([case, coup, None])
    possible_positions = []
    for move in coups_possibles:
        d_board_ai = copy.deepcopy(d_board)
        cases_occupied_ai = copy.deepcopy(cases_occupied_l)
        positions_ai = copy.deepcopy(positions)
        king_moved_ai = copy.deepcopy(king_moved)
        king_side_rook_moved_ai = copy.deepcopy(king_side_rook_moved)
        queen_side_rook_moved_ai = copy.deepcopy(queen_side_rook_moved)
        result = None
        clicked = move[0]
        case_finale = move[1]
        ligne = int(clicked[1])-1
        ligne_f = int(case_finale[1])-1
        for j in range(8):
            if clicked == d_board[ligne][j][0]:
                colonne = j
        for j in range(8):
            if case_finale == d_board[ligne_f][j][0]:
                colonne_f = j
        for i in range(8):
            if d_board[ligne][i][0]==clicked:
                colonne=i
                if d_board_ai[ligne][i][3]==turn and coup_legal(clicked, case_finale, last_move, cases_occupied_ai, checked, d_board_ai, king_moved_ai, king_side_rook_moved_ai, queen_side_rook_moved_ai):
                    if case_finale in cases_occupied_ai:
                        del cases_occupied_ai[case_finale]
                    elif d_board_ai[ligne][colonne][1]=="pion":
                        if ligne_f == ligne+1 and colonne_f == colonne+1:
                            en_passant = d_board_ai[ligne][colonne_f][0]
                            del cases_occupied_ai[en_passant]
                            d_board_ai[ligne][colonne_f][1]=None
                            d_board_ai[ligne][colonne_f][2]=None
                            d_board_ai[ligne][colonne_f][3]=None
                    elif d_board_ai[ligne][colonne][1]=="roi" and d_board_ai[ligne][colonne][0]=="e1" and d_board_ai[ligne_f][colonne_f][0]=="g1":
                        d_board_ai[0][7][1] = None
                        d_board_ai[0][7][2] = None
                        d_board_ai[0][7][3] = None
                        d_board_ai[0][5][1] = "tour"
                        d_board_ai[0][5][2] = "tour2_blanc"
                        d_board_ai[0][5][3] = 1
                        del cases_occupied_ai["h1"]
                        cases_occupied_ai["f1"]="tour2_blanc"
                    elif d_board_ai[ligne][colonne][1]=="roi" and d_board_ai[ligne][colonne][0]=="e1" and d_board_ai[ligne_f][colonne_f][0]=="c1":
                        d_board_ai[0][0][1] = None
                        d_board_ai[0][0][2] = None
                        d_board_ai[0][0][3] = None
                        d_board_ai[0][3][1] = "tour"
                        d_board_ai[0][3][2] = "tour1_blanc"
                        d_board_ai[0][3][3] = 1
                        del cases_occupied_ai["a1"]
                        cases_occupied_ai["d1"]="tour1_blanc"
                    elif d_board_ai[ligne][colonne][1]=="roi" and d_board_ai[ligne][colonne][0]=="e8" and d_board_ai[ligne_f][colonne_f][0]=="g8":
                        d_board_ai[7][7][1] = None
                        d_board_ai[7][7][2] = None
                        d_board_ai[7][7][3] = None
                        d_board_ai[7][5][1] = "tour"
                        d_board_ai[7][5][2] = "tour2_noir"
                        d_board_ai[7][5][3] = 2
                        del cases_occupied_ai["h8"]
                        cases_occupied_ai["f8"]="tour2_noir"
                    elif d_board_ai[ligne][colonne][1]=="roi" and d_board_ai[ligne][colonne][0]=="e8" and d_board_ai[ligne_f][colonne_f][0]=="c8":
                        d_board_ai[7][0][1] = None
                        d_board_ai[7][0][2] = None
                        d_board_ai[7][0][3] = None
                        d_board_ai[7][3][1] = "tour"
                        d_board_ai[7][3][2] = "tour1_noir"
                        d_board_ai[7][3][3] = 2
                        del cases_occupied_ai["a8"]
                        cases_occupied_ai["d8"]="tour1_noir"
                    if d_board_ai[ligne][colonne][1]=="pion" and (ligne_f==7 or ligne_f==0):
                        promotion = move[2]
                        d_board_ai[ligne][colonne][1]=promotion
                    if d_board_ai[ligne][colonne][2]=="roi_blanc":
                        king_moved_ai[0] = True
                    elif d_board_ai[ligne][colonne][2]=="roi_noir":
                        king_moved_ai[1] = True
                    elif d_board_ai[ligne][colonne][2]=="tour1_blanc":
                        queen_side_rook_moved_ai[0] = True
                    elif d_board_ai[ligne][colonne][2]=="tour1_noir":
                        queen_side_rook_moved_ai[1] = True
                    elif d_board_ai[ligne][colonne][2]=="tour2_blanc":
                        king_side_rook_moved_ai[0] = True
                    elif d_board_ai[ligne][colonne][2]=="tour2_noir":
                        king_side_rook_moved_ai[1] = True
                    cases_occupied_ai[case_finale]=cases_occupied_ai[clicked]
                    del cases_occupied_ai[clicked]
                    last_move = [clicked, case_finale, d_board_ai[ligne][colonne][1]]
                    d_board_ai[ligne_f][colonne_f][1]=d_board_ai[ligne][i][1]
                    d_board_ai[ligne_f][colonne_f][2]=d_board_ai[ligne][i][2]
                    d_board_ai[ligne_f][colonne_f][3]=d_board_ai[ligne][i][3]
                    d_board_ai[ligne][i][1] = None
                    d_board_ai[ligne][i][2] = None
                    d_board_ai[ligne][i][3] = None
                    positions_ai.append(d_board_ai[:])
                    check = 0
                    if turn ==1:
                        for case in cases_occupied_ai:
                            if cases_occupied_ai[case]=="roi_noir":
                                case_roi = case
                    if turn ==2:
                        for case in cases_occupied_ai:
                            if cases_occupied_ai[case]=="roi_blanc":
                                case_roi = case
                    for case in cases_occupied_ai:
                        if case_roi in coup_possiblement_legal(case, last_move, cases_occupied_ai, check, d_board_ai, king_moved_ai, king_side_rook_moved_ai, queen_side_rook_moved_ai):
                            check=1
                    if check == 1:
                        if checkmate(case_roi, cases_occupied_ai, last_move, d_board_ai, king_moved_ai, king_side_rook_moved_ai, queen_side_rook_moved_ai):
                            if turn==1:
                                if side==1:
                                    result = "win"
                                else:
                                    result ="lose"
                            else:
                                if side==1:
                                    result="lose"
                                else:
                                    result="lose"
                    else:
                        if stalemate(cases_occupied_ai, turn, d_board_ai, last_move, king_moved_ai, king_side_rook_moved_ai, queen_side_rook_moved_ai):
                            result="draw"
                        elif draw(positions, d_board_ai, cases_occupied_ai):
                            result="draw"
        if result == "win":
            possible_positions.append([d_board_ai, cases_occupied_ai, positions_ai, king_moved_ai, king_side_rook_moved_ai, queen_side_rook_moved_ai,"win", None, move])
        elif result == "lose":
            possible_positions.append([d_board_ai, cases_occupied_ai, positions_ai, king_moved_ai, king_side_rook_moved_ai, queen_side_rook_moved_ai,"lose", None, move])
        elif result == "draw":
            possible_positions.append([d_board_ai, cases_occupied_ai, positions_ai, king_moved_ai, king_side_rook_moved_ai, queen_side_rook_moved_ai,"draw", None, move])   
        else:
            possible_positions.append([d_board_ai, cases_occupied_ai, positions_ai, king_moved_ai, king_side_rook_moved_ai, queen_side_rook_moved_ai,0, 0, move])        
    return possible_positions
                    

def evaluation(d_board):
    white_points = 0
    black_points = 0
    for i in range(8):
        for j in range(8):
            if d_board[i][j][3]==1:
                if d_board[i][j][1]=="pion":
                    white_points += 1
                elif d_board[i][j][1]=="tour":
                    white_points += 5
                elif d_board[i][j][1]=="fou":
                    white_points += 3
                elif d_board[i][j][1]=="cavalier":
                    white_points += 3
                elif d_board[i][j][1]=="reine":
                    white_points += 9
            elif d_board[i][j][3]==2:
                if d_board[i][j][1]=="pion":
                    black_points += 1
                elif d_board[i][j][1]=="tour":
                    black_points += 5
                elif d_board[i][j][1]=="fou":
                    black_points += 3
                elif d_board[i][j][1]=="cavalier":
                    black_points += 3
                elif d_board[i][j][1]=="reine":
                    black_points += 9
    if white_points>black_points:
        return 1
    elif black_points>white_points:
        return 2
    else:
        return 0
    
def chess_ai(last_move, cases_occupied_l, checked, d_board_bis, king_moved, king_side_rook_moved, queen_side_rook_moved, positions, nbr_of_simulations, side):
    #Initialisation
    root = Node([d_board_bis, cases_occupied_l, 0, -1])
    while root.name[2]<nbr_of_simulations:
        node = root
        current_side=side
        #Selection
        while len(node.children)>0:
            j=0
            chosen_node = node.children[j]
            while j<len(node.children)-1 and node.children[j].name[7]==None:
                j+=1
                chosen_node = node.children[j]
            if node.children[j].name[7]!=0 and node.children[j].name[7]!=None:
                if node == root:
                    parent_visited = root.name[2]
                else:
                    parent_visited = node.name[7]
                if current_side==1:
                    best_score = node.children[j].name[6]/node.children[j].name[7] + np.sqrt(2)*(np.sqrt(parent_visited)/node.children[j].name[7])
                else:
                    best_score = (node.children[j].name[7]-node.children[j].name[6])/node.children[j].name[7] + np.sqrt(2)*(np.sqrt(parent_visited)/node.children[j].name[7])
                if len(node.children)>j+1:
                    for i in range(j+1, len(node.children)):
                        if node.children[i].name[7]!=0:
                            if current_side==1 and node.children[i].name[6]/node.children[i].name[7] + np.sqrt(2)*(np.sqrt(parent_visited)/node.children[i].name[7])>best_score:
                                chosen_node = node.children[i]
                                best_score = node.children[i].name[6]/node.children[i].name[7] + np.sqrt(2)*(np.sqrt(parent_visited)/node.children[i].name[7])
                            elif current_side==2 and (node.children[i].name[7]-node.children[i].name[6])/node.children[i].name[7] + np.sqrt(2)*(np.sqrt(parent_visited)/node.children[i].name[7])>best_score:
                                chosen_node = node.children[i]
                                best_score = (node.children[i].name[7]-node.children[i].name[6])/node.children[i].name[7] + np.sqrt(2)*(np.sqrt(parent_visited)/node.children[i].name[7])
                        else:
                            chosen_node = node.children[i]
                            break
            elif node.children[j].name[7]==None:
                return(node.children[0].name[8])
            node = chosen_node
            if current_side ==1:
                current_side=2
            else:
                current_side=1
        #Expansion
        if node == root:
            coups_possibles = possible_next_positions(node.name[0], last_move, node.name[1], checked, king_moved, king_side_rook_moved, queen_side_rook_moved, side, positions, current_side)
        else:
            coups_possibles = possible_next_positions(node.name[0], last_move, node.name[1], checked, node.name[3], node.name[4], node.name[5], side, node.name[2], current_side)
        for move in coups_possibles:
            new = Node(move, parent=node)
        #Evaluation
        chosen_move = random.randint(0, len(coups_possibles)-1)
        exploration_node = node.children[chosen_move]
        if evaluation(exploration_node.name[0])==1:
            my_eval=1
        elif evaluation(exploration_node.name[0])==0:
            my_eval=0.5
        else:
            my_eval=0
        exploration_node.name[6]+= my_eval
        exploration_node.name[7]+=1
        #Backpropagation:
        parent_node = exploration_node.parent
        while parent_node.name[3]!=-1:
            parent_node.name[6]+=my_eval
            parent_node.name[7]+=1
            parent_node = parent_node.parent
        root.name[2]+=1
    most_explored = 0
    chosen_move = None
    explored = []
    for child in root.children:
        if child.name[7]>most_explored:
            chosen_move = child.name[8]
        explored.append((child.name[7], child.name[8]))
    print(explored)
    return chosen_move

cases_occupied = {"a1": "tour1_blanc", "a2": "pion1_blanc", "a7": "pion1_noir", "a8": "tour1_noir", "b1": "cavalier1_blanc", "b2": "pion2_blanc", "b7": "pion2_noir", "b8": "cavalier1_noir", "c1": "fou_n_blanc", "c2": "pion3_blanc", "c7": "pion3_noir", "c8": "fou_b_noir", "d1": "reine_blanc", "d2": "pion4_blanc", "d7": "pion4_noir", "d8": "reine_noir", "e1": "roi_blanc", "e4": "pion5_blanc", "e7": "pion5_noir", "e8": "roi_noir", "f1": "fou_b_blanc", "f2": "pion6_blanc", "f7": "pion6_noir", "f8": "fou_n_noir", "g1": "cavalier2_blanc", "g2": "pion7_blanc", "g7": "pion7_noir", "g8": "cavalier2_noir", "h1": "tour2_blanc", "h2": "pion8_blanc", "h7": "pion8_noir", "h8": "tour2_noir"}

#Nom de la case, type de pièce, nom de la pièce
d_board = [[["a1", "tour", "tour1_blanc", 1],["b1", "cavalier", "cava]ier1_blanc", 1],["c1","fou", "fou_n_blanc", 1],["d1", "reine", "reine_blanc", 1],["e1", "roi", "roi_blanc", 1],["f1", "fou", "fou_b_blanc", 1],["g1", "cavalier", "cavalier2_blanc", 1],["h1", "tour", "tour2_blanc", 1]],
         [["a2", "pion", "pion1_blanc", 1],["b2", "pion", "pion2_blanc", 1],["c2", "pion", "pion3_blanc", 1],["d2", "pion", "pion4_blanc", 1],["e2", None, None, None],["f2", "pion", "pion6_blanc", 1],["g2", "pion", "pion7_blanc", 1], ["h2", "pion", "pion8_blanc", 1]],
         [["a3", None, None, None],["b3", None, None, None],["c3", None, None, None],["d3", None, None, None],["e3", None, None, None],["f3", None, None, None],["g3", None, None, None],["h3", None, None, None]],
         [["a4", None, None, None],["b4", None, None, None],["c4", None, None, None],["d4", None, None, None],["e4", "pion", "pion5_blanc", 1],["f4", None, None, None],["g4", None, None, None],["h4", None, None, None]],
         [["a5", None, None, None],["b5", None, None, None],["c5", None, None, None],["d5", None, None, None],["e5", None, None, None],["f5", None, None, None],["g5", None, None, None],["h5", None, None, None]],
         [["a6", None, None, None],["b6", None, None, None],["c6", None, None, None],["d6", None, None, None],["e6", None, None, None],["f6", None, None, None],["g6", None, None, None],["h6", None, None, None]],
         [["a7", "pion", "pion1_noir", 2],["b7", "pion", "pion2_noir", 2],["c7", "pion", "pion3_noir", 2],["d7", "pion", "pion4_noir", 2],["e7", "pion", "pion5_noir", 2],["f7", "pion", "pion6_noir", 2],["g7", "pion", "pion7_noir", 2],["h7", "pion", "pion8_noir", 2]],
         [["a8", "tour", "tour1_blanc", 2],["b8", "cavalier", "cavalier1_noir", 2],["c8", "fou", "fou_b_noir", 2],["d8", "reine", "reine_noir", 2],["e8", "roi", "roi_noir", 2],["f8", "fou", "fou_n_noir", 2],["g8", "cavalier", "cavalier2_noir", 2],["h8", "tour", "tour2_noir", 2]]]

#print(coup_possiblement_legal("c7", None, cases_occupied, 0, d_board, [False, False], [False, False], [False, False]))
#print(coup_legal("c7", "c5", None, cases_occupied, 0, d_board, [False, False], [False, False], [False, False]))
"""l = possible_next_positions(d_board, None, cases_occupied, 0, [False, False], [False, False], [False, False], 1, [], 1)
for i in range(len(l)):
    print(l[i][4])"""